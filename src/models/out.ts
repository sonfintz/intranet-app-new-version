export interface IOut {
  id?: number;
  recibo: number;
  bruto: number;
  tara: number;
  neto: number;
  observacion?: string;
  fecha: string;
  fecha_registro: string;
  transportadora: 1 | 0;

  vehiculo: {
    id?: number,
    placa: string,
    tipos_vehiculo_id: number
  };
  conductor: {
    id?: number,
    dni: number,
    nombre: string
  };

  patios_id: number,
  pilas_id: number,
  materiales_id: number,
  clientes_id: number,
  minas_id: number
}
