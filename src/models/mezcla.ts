export interface IMezcla {
  id?: number,
  observacion?: string,
  recibo: number,

  numero_orden: number,
  total_rondas: number,
  tiempo_cargador: number,
  capacidad_cargador: number,
  total_tonelada_mezcla: number,

  patios_id: number,
  sedes_id?: number,
  fecha: string,

}
