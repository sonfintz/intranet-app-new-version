import {NetworkProvider} from "../providers/network/network";
import {Md5} from "ts-md5";

export interface IAuthCredentials {
  correo: string;
  pin?: string;
  clave?: string;
}

export class AuthCredentials {
  correo: string;
  pin: string;
  clave: string;

  constructor(credentials: IAuthCredentials) {
    this.correo = credentials.correo.toUpperCase();
    if (credentials.pin) {
      this.pin = credentials.pin.length < 32 ? (Md5.hashStr(credentials.pin) as string) : credentials.pin;
    }
    if (credentials.clave) {
      this.clave = credentials.clave.length < 32 ? (Md5.hashStr(credentials.clave) as string) : credentials.clave;
    }
  }

  toObject(): IAuthCredentials {
    let object: IAuthCredentials = {
      correo: this.correo,
    };
    if (NetworkProvider.onLine) {
      object.clave = this.clave;
    } else {
      object.pin = this.pin;
    }
    return object;
  }
}
