export interface IEntry {
  id?: number,
  bruto: number,
  tara: number,
  neto: number,
  observacion?: string,
  fecha_registro: string,

  vehiculo: {
    id?: number,
    placa: string,
    tipos_vehiculo_id: number
  },
  conductor: {
    id?: number,
    dni: number,
    nombre: string
  }

  productos_id: number,
  pilas_id: number,
  patios_id: number,
  mantos_id: number,
  sedes_id?: number,
  fecha: string,
  vc?: {
    id?: number
    vehiculos_id?: number
    conductores_id?: number
  }
}
