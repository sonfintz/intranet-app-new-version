export interface IActivity {
  id: number;
  tabla: string;
  llave?: number;
  columna?: string;
  fecha?: string;
  estado?: number;
}
