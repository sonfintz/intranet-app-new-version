export function mapRows(map): any[] {
  const lists = [];
  for (let i = 0; i < map.rows.length ; i++)
    lists.push(map.rows.item(i));
  return lists;
}

export function findOne(rows) {
  return rows.rows.item(0);
}

export function dbErrorHandler(e) {
  console.error('ERROR QUERY', e);
  return null;
};

export function dbErrorHandlerFunction(message?) {
  console.error(message);
  return dbErrorHandler
}
