import * as _ from "lodash";

export function formatObject(data, split = '_') {
  const newObj = {};
  let key: string;
  for (key in data) {
    const subKey = key.split(split);
    if(subKey.length > 1) {
      const newKey = subKey[0];
      newObj[newKey] = !_.isObject(newObj[newKey]) ? {} : newObj[subKey[0]];
      newObj[newKey][subKey[1]] = data[key];
    } else {
      newObj[key] = data[key];
    }
  }
  return newObj;
}
