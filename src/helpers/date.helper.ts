import * as moment from "moment";

export function momentEs(fecha: any, format: string) {
  return moment(fecha).locale('es').format(format);
}

export function datetime(fecha?: any) {
  return moment(fecha).format('YYYY-MM-DD HH:mm:ss');
}
