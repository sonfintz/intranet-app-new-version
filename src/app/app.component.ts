import {Component, OnInit} from '@angular/core';
import {Loading, LoadingController, Platform, ToastController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {TabPage} from "../pages/tab/tab";
import {AuthService} from "../providers/_core/auth-service/auth-service";
import {SyncProvider} from "../providers/sync/sync";
import {LoginPage} from "../pages/login/login";
import {Network} from "@ionic-native/network";
import {NetworkProvider} from "../providers/network/network";
import {DatabaseProvider} from "../providers/database/database";
import {MainPage} from "../pages/main/main";
import {timer} from "rxjs/observable/timer";


@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  rootPage: any = AuthService.isAuthenticated ? MainPage : LoginPage;
  // rootPage:any = AuthService.isAuthenticated ? MezclasPage : LoginPage ;
  rootPage2: any = TabPage;
  private loading: Loading;
  authEvent;
  public showSplash = true;



  get onLine() {
    return NetworkProvider.onLine;
  }


  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              network: Network,
              private database: DatabaseProvider,
              private toast: ToastController,
              private auth: AuthService,
              private preload: LoadingController,
              private sync: SyncProvider) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      if(!localStorage.getItem('user-data')){
        timer(10000).subscribe(()=> this.showSplash = false);
      }
      else{
        this.showSplash = false;
      }

      if (splashScreen) {
        setTimeout(() => {
          splashScreen.hide();
        }, 100);
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.database.dbReady.subscribe((flag) => {
        if (flag) {
          statusBar.styleDefault();
          splashScreen.hide();
        }
      });
    });
    NetworkProvider.onLine = true;
    network.onConnect().subscribe(() => this.onConnect());
    network.onDisconnect().subscribe(() => this.onDisconnect());
  }

  ngOnInit() {
    this.authEvent = this.auth.onAuthenticateEvent.share();
    this.authEvent.subscribe((a) => {
      if (a ) {
        if(this.onLine) {
          this.sync.socketConnect();
          this.syncProccess();
        }
      }
    });
  }

  preloader() {
    this.loading = this.preload.create({
      content: 'Almacenando Datos por favor espere',
    });
  }

  onConnect() {
    const alert = this.toast.create({
      message:'conectando',
      position: 'bottom',
      duration: 2000,
      showCloseButton: true
    });
    alert.present();

    console.warn('\n\n\nIs Online\n\n\n');
    NetworkProvider.onLine = true;
    this.syncProccess();
  }

  onDisconnect() {
    const alert = this.toast.create({
      message:'Sin conexion',
      position: 'bottom',
      duration: 2000,
      showCloseButton: true
    });
    alert.present();
    console.log('Is Offline');
    NetworkProvider.onLine = false;
    // this.sync.socket.disconnect();
  }

  syncProccess() {
    if (this.onLine && AuthService.isAuthenticated) {
      this.preloader();
      this.sync.upload().then(
        () => {
          this.sync.download()
            .then(() => {
              // this.sync.reconnect();
              this.loading.dismissAll()
            })
            .catch(reason => {
              console.log('FINISH ERROR', reason);
              this.loading.dismissAll();
            });
        },
        (e) => {
          console.log('\n\n\n', e);
        }
      );
    }
  }

}

