import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {Api} from '../providers/_core/api/api';
import {LoginPage} from "../pages/login/login";
import {AuthService} from "../providers/_core/auth-service/auth-service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {UsersProvider} from "../providers/users/users";
import {MainPage} from "../pages/main/main";
import {TabPage} from "../pages/tab/tab";
import {EntradaPage} from "../pages/entrada/entrada";
import {SalidaPage} from "../pages/salida/salida";
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {SQLitePorter} from "@ionic-native/sqlite-porter";
import {SQLite} from "@ionic-native/sqlite";
import {Network} from "@ionic-native/network";
import {SyncProvider} from '../providers/sync/sync';
import {BearerInterceptor} from "../config/interceptors/bearer";
import {DatabaseProvider} from "../providers/database/database";
import {AddPlacaPage} from "../pages/add-placa/add-placa";
import {UsersDatabase} from "../providers/database/users.database";
import {ProductsDatabase} from "../providers/database/products.database";
import {ClientesDatabase} from "../providers/database/clientes.database";
import {MaterialesDatabase} from "../providers/database/materiales.database";
import {Tipos_vehiculosDatabase} from "../providers/database/tipos_vehiculos.database";
import {VehiculosDatabase} from "../providers/database/vehiculos.database";
import {ConductoresDatabase} from "../providers/database/conductores.database";
import {Vehiculos_conductoresDatabase} from "../providers/database/vehiculos_conductores.database";
import {PatiosDatabase} from "../providers/database/patios.database";
import {PilasDatabase} from "../providers/database/pilas.database";
import {MinasDatabase} from "../providers/database/minas.database";
import {MantosDatabase} from "../providers/database/mantos.database";
import {Entrada_bascula} from "../providers/database/entrada_bascula";
import {Salidas_bascula} from "../providers/database/salidas_bascula";
import {AuthorizationInterceptor} from "../config/interceptors/authorization.interceptor";
import {SocketIoConfig, SocketIoModule} from "ngx-socket-io";
import {MezclasPage} from "../pages/mezclas/mezclas";
import {Mezcla_bascula} from "../providers/database/mezcla_bascula";

const config: SocketIoConfig = {url: 'http://186.116.198.6:9888', options: {autoConnect: false} };

const pages = [
  LoginPage,
  MyApp,
  HomePage,
  MainPage,
  TabPage,
  EntradaPage,
  SalidaPage,
  AddPlacaPage,
  MezclasPage
];

const databaseProviderS: any[] = [
  UsersDatabase,
  DatabaseProvider,
  ProductsDatabase,
  ClientesDatabase,
  MaterialesDatabase,
  Tipos_vehiculosDatabase,
  VehiculosDatabase,
  ConductoresDatabase,
  Vehiculos_conductoresDatabase,
  PatiosDatabase,
  PilasDatabase,
  MinasDatabase,
  MantosDatabase,
  Entrada_bascula,
  Salidas_bascula,
  Mezcla_bascula
];

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ...pages,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      monthNames: ['enero','febrero','marzo','abril','mayo','junio','julio', 'agosto','septiembre','octubre','noviembre','diciembre'],
      monthShortNames: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic',],
      dayNames: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
      dayShortNames: ['do', 'lu', 'ma','mi','ju','vi','sa', ],
    }),
    HttpClientModule,
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ...pages
  ],
  providers: [
    SQLitePorter,
    SQLite,
    Network,
    StatusBar,
    SplashScreen,
    Api,
    Network,
    AuthService,
    UsersProvider,
    SyncProvider,
    ...databaseProviderS,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HTTP_INTERCEPTORS, useClass: BearerInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true},
  ]
})
export class AppModule {}
