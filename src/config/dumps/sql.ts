export const dump = `

-- Tabla usuarios
DROP TABLE IF EXISTS usuarios;
CREATE TABLE IF NOT EXISTS usuarios (
  usuario_id INT PRIMARY KEY ASC,
  usuario_nombre VARCHAR(128) NOT NULL,
  usuario_correo VARCHAR(128) NOT NULL,
  usuario_pin VARCHAR(128) DEFAULT NULL,
  usuario_creado VARCHAR(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  usuario_actualizado VARCHAR(128) DEFAULT CURRENT_TIMESTAMP,
  persona_id INTEGER NOT NULL,
  persona_nombre VARCHAR(128) NOT NULL,
  persona_creado VARCHAR(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  persona_actualizado VARCHAR(128) DEFAULT CURRENT_TIMESTAMP,
  sede_id INTEGER NOT NULL,
  sede_nombre VARCHAR(128) NOT NULL,
  empresa_id INTEGER NOT NULL,
  empresa_nombre VARCHAR(128) NOT NULL
);
-- Tabla productos
DROP TABLE IF EXISTS productos; 
CREATE TABLE IF NOT EXISTS productos (
  id INTEGER NOT NULL PRIMARY KEY,
  codigo varchar(32) NOT NULL,
  nombre varchar(128) NOT NULL,
  detalle text,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL DEFAULT NULL
);

-- Tabla clientes
DROP TABLE IF EXISTS clientes;
CREATE TABLE IF NOT EXISTS clientes (
  id INTEGER NOT NULL PRIMARY KEY,
  nombre varchar(128) DEFAULT NULL,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) DEFAULT NULL DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL DEFAULT NULL,
  empresas_id INTEGER NOT NULL
);

DROP TABLE IF EXISTS materiales;
CREATE TABLE IF NOT EXISTS materiales (
  id INTEGER NOT NULL,
  nombre varchar(128) NOT NULL,
  detalle text,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL DEFAULT CURRENT_TIMESTAMP,
  sedes_id INTEGER NOT NULL,
  PRIMARY KEY(id, sedes_id)
);


-- Tipos tipos vehiculo
DROP TABLE IF EXISTS tipos_vehiculo;
 CREATE TABLE IF NOT EXISTS tipos_vehiculo (
 id INTEGER NOT NULL PRIMARY KEY,
 nombre varchar(128) NOT NULL,
 detalle text,
 creado timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 actualizado timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 eliminado timestamp NULL DEFAULT NULL
 );

-- Vehiculos
DROP TABLE IF EXISTS  vehiculos;
CREATE TABLE IF NOT EXISTS vehiculos (
  id INTEGER NOT NULL PRIMARY KEY,
  placa varchar(6) NOT NULL UNIQUE,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL DEFAULT NULL,
  tipos_vehiculo_id INTEGER NOT NULL,
  uploaded int(1) not null default 0,
  confirmed int(1) not null default 0,
  FOREIGN KEY (tipos_vehiculo_id) REFERENCES tipos_vehiculo (id)
);

-- conductores
DROP TABLE IF EXISTS  conductores;
CREATE TABLE IF NOT EXISTS conductores (
  id INTEGER NOT NULL PRIMARY KEY,
  nombre varchar(128) NOT NULL,
  dni varchar(16) NOT NULL UNIQUE,
  detalle text,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL DEFAULT NULL,
  uploaded int(1) not null default 0,
  confirmed int(1) not null default 0
);

-- vehiculos conductores
DROP TABLE IF EXISTS vehiculos_conductores;
CREATE TABLE IF NOT EXISTS vehiculos_conductores (
  id INT NOT NULL,
  vehiculos_id INT NOT NULL,
  conductores_id INT NOT NULL,
  fecha varchar(128) NOT NULL,
  estado INT(1) NOT NULL DEFAULT 1,
  uploaded int(1) not null default 0,
  confirmed int(1) not null default 0,
  PRIMARY KEY (id, vehiculos_id, conductores_id),
  FOREIGN KEY (vehiculos_id) REFERENCES vehiculos (id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (conductores_id) REFERENCES conductores (id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- patios
DROP TABLE IF EXISTS patios ;
CREATE TABLE IF NOT EXISTS patios (
  id INT NOT NULL,
  sedes_id INT NOT NULL,
  nombre VARCHAR(128) NOT NULL,
  detalle TEXT NULL,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL,
  PRIMARY KEY (id, sedes_id)
);

-- pilas
DROP TABLE IF EXISTS pilas;
CREATE TABLE IF NOT EXISTS pilas (
  id INT NOT NULL,
  patios_id INT NOT NULL,
  sedes_id INT NOT NULL,
  nombre VARCHAR(128) NOT NULL,
  detalle TEXT NULL,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL,
  PRIMARY KEY (id, patios_id, sedes_id),
  FOREIGN KEY (patios_id , sedes_id) REFERENCES patios (id , sedes_id) ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- MINAS
DROP TABLE IF EXISTS minas;
CREATE TABLE IF NOT EXISTS minas (
  id INTEGER NOT NULL PRIMARY KEY,
  nombre varchar(128) NOT NULL,
  detalle text,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL DEFAULT NULL,
  proveedores_id INTEGER NOT NULL,
  empresas_id INTEGER NOT NULL
);

-- MANTOS
DROP TABLE IF EXISTS mantos;
CREATE TABLE IF NOT EXISTS mantos (
  id INTEGER NOT NULL PRIMARY KEY,
  nombre varchar(128) NOT NULL,
  detalle text,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL DEFAULT NULL,
  minas_id INTEGER NOT NULL,
  FOREIGN KEY (minas_id) REFERENCES minas (id)
);


-- Entradas de bascula
DROP TABLE IF EXISTS entradas_bascula;
CREATE TABLE IF NOT EXISTS entradas_bascula (
  id INT NOT NULL,
  neto DECIMAL NOT NULL,
  bruto DECIMAL NOT NULL,
  tara DECIMAL NULL,
  fecha varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  fecha_registro varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  observacion TEXT NULL,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL,
  productos_id INT NOT NULL,
  pilas_id INT NOT NULL,
  patios_id INT NOT NULL,
  sedes_id INT NOT NULL,
  usuarios_id INT NOT NULL,
  vc_id INT NOT NULL,
  vc_vehiculos_id INT NOT NULL,
  vc_conductores_id INT NOT NULL,
  mantos_id INT NOT NULL,
  uploaded INT(1) NOT NULL DEFAULT 0,
  confirmed INT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (productos_id) REFERENCES productos (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (pilas_id , patios_id , sedes_id)  REFERENCES pilas (id , patios_id , sedes_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (usuarios_id) REFERENCES usuarios (usuario_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (vc_id , vc_vehiculos_id , vc_conductores_id) REFERENCES vehiculos_conductores (id , vehiculos_id , conductores_id) ON DELETE CASCADE ON UPDATE CASCADE
  FOREIGN KEY (mantos_id) REFERENCES mantos (id) ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- salidas de bascula
DROP TABLE IF EXISTS salidas_bascula;
CREATE TABLE IF NOT EXISTS salidas_bascula (
  id INT NOT NULL,
  neto DECIMAL NOT NULL,
  bruto DECIMAL NOT NULL,
  tara DECIMAL NULL,
  fecha varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  fecha_registro varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  transportadora INTEGER(1) NOT NULL,
  observacion TEXT NULL,
  creado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  actualizado varchar(128) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  eliminado varchar(128) NULL,
  recibo VARCHAR(128) NOT NULL,
  clientes_id INT NOT NULL,
  materiales_id INT NOT NULL,
  sedes_id INT NOT NULL,
  usuarios_id INT NOT NULL,
  minas_id INT NOT NULL,
  vc_id INT NOT NULL,
  vc_vehiculos_id INT NOT NULL,
  vc_conductores_id INT NOT NULL,
  pilas_id INT NOT NULL,
  patios_id INT NOT NULL,
  uploaded INT(1) NOT NULL DEFAULT 0,
  confirmed INT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (clientes_id) REFERENCES clientes (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (pilas_id, patios_id) REFERENCES pilas (id, patios_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (materiales_id , sedes_id) REFERENCES materiales (id , sedes_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (vc_id , vc_vehiculos_id , vc_conductores_id) REFERENCES vehiculos_conductores (id , vehiculos_id , conductores_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (minas_id) REFERENCES minas (id) ON DELETE CASCADE ON UPDATE CASCADE
);
`;
