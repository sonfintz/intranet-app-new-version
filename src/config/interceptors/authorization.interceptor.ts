import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {AuthService} from "../../providers/_core/auth-service/auth-service";
import {NetworkProvider} from "../../providers/network/network";

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).map((event) => {
      if(event instanceof HttpResponse) {
        switch (event.status) {
          case 200:
            console.log(event.status);
            break;
          case 0:
            console.log(event.status);
            NetworkProvider.onLine = false;
            break;
          case 401:
            console.log('NO AUTORIZADO', event);
            this.auth.logout().subscribe((e) => {
              console.log('CERRAR SESION', e);
            });
            break;
        }
      }
      return event;
    });
  }
}
