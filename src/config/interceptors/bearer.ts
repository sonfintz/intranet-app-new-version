import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {AuthService} from "../../providers/_core/auth-service/auth-service";

@Injectable()
export class BearerInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('URL', req.url);

    if (AuthService.isAuthenticated) {
      const reqClone = req.clone({headers: req.headers.set('authorization', AuthService.getBearer)});
      return next.handle(reqClone);
    }
    return next.handle(req);
  }
}
