import {ChangeDetectorRef, Component} from '@angular/core';
import {AlertController, Events, NavController, NavParams, ToastController} from 'ionic-angular';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from "@angular/forms";
import {AddPlacaPage} from "../add-placa/add-placa";
import * as _ from 'lodash';
import {Vehiculos_conductoresDatabase} from "../../providers/database/vehiculos_conductores.database";
import {MinasDatabase} from "../../providers/database/minas.database";
import {MantosDatabase} from "../../providers/database/mantos.database";
import {PatiosDatabase} from "../../providers/database/patios.database";
import {PilasDatabase} from "../../providers/database/pilas.database";
import {ProductsDatabase} from "../../providers/database/products.database";
import {VehiculosDatabase} from "../../providers/database/vehiculos.database";
import {Entrada_bascula} from "../../providers/database/entrada_bascula";
import {IEntry} from "../../models/entry";
import {datetime} from "../../helpers/date.helper";
import {NetworkProvider} from "../../providers/network/network";
import {ConductoresDatabase} from "../../providers/database/conductores.database";
import * as moment from "moment";

export function weight(msg: string = 'El peso bruto debe ser mayor que el peso tara.'): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const gross = Number(control.get('bruto').value);
    const tare = Number(control.get('tara').value);
    return Boolean(gross <= tare) ? {'weight': msg} : null;
  };
}

@Component({
  selector: 'page-entrada',
  templateUrl: 'entrada.html',
})
export class EntradaPage {
  edit: boolean;
  entryForm: FormGroup;
  data: any = {
    vehiculos: [],
    patios: [],
    pilas: [],
    minas: [],
    productos: [],
    mantos: [],
    conductores: []
  };
  fecha = moment();
  hora = moment();

  parentCallback;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public alertCtrl: AlertController,
              public vehiculos_conductores: Vehiculos_conductoresDatabase,
              public origen: MinasDatabase,
              public mantos: MantosDatabase,
              public areas: PatiosDatabase,
              public pilas: PilasDatabase,
              public product: ProductsDatabase,
              public vehiculo: VehiculosDatabase,
              public entrada: Entrada_bascula,
              public conductores: ConductoresDatabase,
              public events: Events,
              public toastCtrl: ToastController,
              public cd: ChangeDetectorRef
  ) {
    Promise.all([
      this.vehiculo.findAll(),
      this.areas.findAll(),
      this.origen.findAll(),
      this.product.findAll(),
    ])
      .then((select: any[]) => {
        console.log('ENTRADAS RESULT', select);
        this.data.vehiculos = select[0];
        this.data.patios = select[1];
        this.data.minas = select[2];
        this.data.productos = select[3];
      })
      .catch((e) => {
        console.log('CATCH', e);
      });
    this.createMyForm();
    this.parentCallback = this.navParams.get('callback');
  }

  timeChange($event){
    console.log($event);
    this.hora = moment($event);
  }

  dateChange($event){
    console.log($event);
    this.fecha = moment($event);
  }

  get color() {
    return Boolean(NetworkProvider.onLine) ? "intranet" : "black";
  }

  ionViewDidLoad() {
    const toEdit = this.navParams.get('edit');
    if (Boolean(toEdit)) {
      this.entryForm.get('id').setValue(toEdit.id);
      this.placaField.setValue(toEdit.vehiculo_placa);
      this.dniField.setValue(toEdit.conductor_dni);
      this.productField.setValue(toEdit.producto_nombre);
      this.origenField.setValue(toEdit.mina_nombre);
      this.mantleField.setValue(toEdit.manto_nombre);
      this.patiosField.setValue(toEdit.patio_nombre);
      this.pileField.setValue(toEdit.pila_nombre);
      this.brutoField.setValue(toEdit.bruto);
      this.taraFields.setValue(toEdit.tara);
      console.log('TO EDIT', toEdit, this.entryForm);
    } else {
      const x: Array<string> = ['productos', 'vehiculos', 'patios', 'pilas', 'minas', 'mantos'];
      this.events.subscribe('new-data', async (data: string) => {
        if (x.includes(data)) {
          switch (data) {
            case 'productos':
              this.data.productos = await this.product.findAll();
              break;
            case 'vehiculos':
              this.data.vehiculos = await this.vehiculo.findAll();
              break;
            case 'patios':
              this.data.patios = await this.areas.findAll();
              break;
            case 'pilas':
              this.data.pilas = await this.pilas.findAll();
              break;
            case 'minas':
              this.data.minas = await this.origen.findAll();
              break;
            case 'mantos':
              this.data.mantos = await this.mantos.findAll();
              break;
          }
          console.log('subscribed-on-new-data', data, this.data);
        }
        this.doToast(`Actualizando datos`);
      });
    }
  }

  dateParse() {
    return this.fecha
      .add(this.hora.hour(), 'hours')
      .add(this.hora.minute(), 'minutes')
      .format('YYYY-MM-DD HH:mm:ss');
  }

  saveData() {
    if (this.entryForm.valid) {
      const form = this.entryForm.value;
      form.fecha_registro = this.dateParse();
      if (this.edit) {
        console.log(this.edit);
        this.entrada.update(form.id, form.tara, form.bruto).then(
          (data) => {
            const alert = this.toastCtrl.create({
              message: 'Entrada Actualizada',
              position: 'bottom',
              duration: 2000,
              showCloseButton: true
            });
            this.cd.detectChanges();
            alert.present();
            console.log('ENTRADA UPDATED', data);
            this.parentCallback(data).then(() => this.navCtrl.pop());
          },
          error => {
            console.log('ERROR ON UPDATED ENTRY', error);
          });
      } else {
        console.log('SARAMAMBICHE', form);
        const body: IEntry = {
          bruto: form.bruto,
          neto: form.neto,
          tara: form.tara,
          observacion: form.observacion,
          conductor: {
            id: form.dni.id,
            dni: form.dni.dni,
            nombre: form.dni.nombre
          },
          vehiculo: {
            id: form.placa.id,
            placa: form.placa.placa,
            tipos_vehiculo_id: form.placa.tipos_vehiculo_id
          },
          fecha: datetime(),
          fecha_registro: form.fecha_registro,
          patios_id: form.patio.id,
          pilas_id: form.pila.id,
          productos_id: form.producto.id,
          mantos_id: form.manto.id,
          vc: form.placa.vc,
        };
        console.log('BODY', form);
        this.entrada.save(body).then(
          (data) => {
            const alert = this.toastCtrl.create({
              message: 'Entrada Almacenada',
              position: 'bottom',
              duration: 2000,
              showCloseButton: true
            });
            alert.present();
            console.log('ENTRADA SAVED', data);
            this.parentCallback(data).then(() => this.navCtrl.pop());
          },
          error => {
            console.log('ERROR ON SAVE ENTRY', error);
          }
        );
      }
      // } else {
      //   for (let x in this.entryForm.controls) {
      //     console.warn(x,this.entryForm.controls[x].errors);
      //   }
    }
  }

  private createMyForm() {
    this.entryForm = this.formBuilder.group({
      placa: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6)
      ])),
      dni: [null, Validators.compose([
        Validators.required,
      ])],
      producto: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])],
      origen: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(15)
      ])],
      manto: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])],
      patio: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])],
      pila: [null, Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ])],
      fecha_registro: null,
      hora_registro: null,
      bruto: [null, Validators.compose([
        Validators.required, Validators.min(0),
        Validators.max(75)
      ])],
      tara: [null, Validators.compose([
        Validators.min(0),
        Validators.max(75)
      ])],
      neto: [null, Validators.compose([Validators.min(1)])],
      observacion: new FormControl(null,),
      id: new FormControl(null)
    }, {
      validator: weight()
    });
    this.entryForm.get('origen').valueChanges
      .subscribe((value: any) => {
        if (_.isObject(value)) {
          this.mantos.findByField('minas_id', value.id)
            .then(
              (data) => {
                console.log('LLEGO', data);
                this.data.mantos = data;
              },
              reason => {
                console.log('ERROR ON VALUE CHANGES ORIGEN', reason);
              }
            );
        }
      });
    this.entryForm.get('patio').valueChanges
      .subscribe((value: any) => {
        if (_.isObject(value)) {
          this.pilas.findByField('patios_id', value.id)
            .then((data) => {
              console.log('LLEGO', data);
              this.data.pilas = data;
            }).catch((data) => {
            console.error('No llego', data);
          });
        }
      });
    this.entryForm.get('placa').valueChanges.subscribe((value: any) => {
      if (_.isObject(value)) {
        console.log('INNER', value);
        if (!value.hasOwnProperty('conductor')) {
          this.vehiculos_conductores.getDriver(value.id)
            .then(
              (r) => this.entryForm.get('dni').setValue(r),
              (r) => console.log('error Drivers', r));
        }
        else {
          this.entryForm.get('dni').setValue(value.conductor);
        }
      } else {
        this.entryForm.get('dni').setValue(null);
      }
    });
    this.entryForm.get('bruto').valueChanges.subscribe((val) => {
      if (isNaN(val)) {
        this.entryForm.get('bruto').setErrors({nan: 'no es un número'});
      }
      const neto = val - this.entryForm.get('tara').value;
      if (neto > 0) {
        this.entryForm.get('neto').setValue(neto);
      } else {
        this.entryForm.get('neto').setErrors({invalidNet: 'peso neto debe ser positivo.'});
      }
    });
    this.entryForm.get('tara').valueChanges.subscribe((val) => {
      if (isNaN(val)) {
        this.entryForm.get('tara').setErrors({nan: 'no es un número'});
      }
      const neto = this.entryForm.get('bruto').value - val;
      if (neto > 0) {
        this.entryForm.get('neto').setValue(neto);
      } else {
        this.entryForm.get('neto').setErrors({invalidNet: 'peso neto debe ser positivo.'});
      }
    });
    this.entryForm.valueChanges
      .debounceTime(400)
      .subscribe(data => this.onValueChanged(data));
    this.entryForm.get('id').valueChanges.subscribe((v) => {
      this.edit = Boolean(v);
    });
  }

  formErrors = {
    'placa': [],
    'bruto': [],
  };

  validationMessages = {
    'placa': {
      'minlength': 'Minimo 6 digitos'
    },
    'bruto': {
      'max': 'Maximo 75 Toneladas',
    },
  };

  onValueChanged(data?: any) {
    if (!this.entryForm) {
      return;
    }
    const form = this.entryForm;
    for (const field in this.formErrors) {
      // Limpiamos los mensajes anteriores
      this.formErrors[field] = [];
      this.entryForm[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  get brutoField() {
    return this.entryForm.get('bruto');
  }

  get taraFields() {
    return this.entryForm.get('tara');
  }

  get productField() {
    return this.entryForm.get('producto');
  }

  get placaField() {
    return this.entryForm.get('placa');
  }

  get mantleField() {
    return this.entryForm.get('manto');
  }

  get origenField() {
    return this.entryForm.get('origen');
  }

  get patiosField() {
    return this.entryForm.get('patio')
  }

  get areasField() {
    return this.entryForm.get('patio');
  }

  get pileField() {
    return this.entryForm.get('pila');
  }

  get dniField() {
    return this.entryForm.get('dni');
  }

  get netWeight() {
    return this.entryForm.get('neto');
  }

  // PRODUCTO ///////////////////////////////////////////////////////////////////////////////
  doProducto() {
    // console.log('PRODUCTOS', this.producto);
    let alert = this.alertCtrl.create();
    alert.setTitle('Producto');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.productField.value) ?
        ( // Es ub objeto?
          _.isObject(this.productField.value) ?
            this.productField.value.nombre : // de ser así obtengo su nombre
            this.productField.value // de lo contrario debe ser una string
        ) : '' /* en caso de contener un valor falso se guarda una cadena de texto vacía*/
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    console.log('VALUE TO SEARCH', value);
    const results = this.data.productos.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    console.log('RESULTS TO SEARCH', results);
    if (results.length > 0) {
      results.forEach((item) => {
        console.log('RESULTS VALUE FOREACH', value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Productos no encontrados');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.productField.setValue(data);
      }
    });

    alert.present().then();
  }

  // FIN PRODUCTO ///////////////////////////////////////////////////////////////////////////

  //PLACA ///////////////////////////////////////////////////////////////////////////////
  doPlaca() {
    // console.log('PLACA', this.placa);
    let alert = this.alertCtrl.create({
      title: 'Vehículos'
    });
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.placaField.value) ?
        ( // Es ub objeto?
          _.isObject(this.placaField.value) ?
            this.placaField.value.placa : // de ser así obtengo su nombre
            this.placaField.value // de lo contrario debe ser una string
        ) : '' /* en caso de contener un valor falso se guarda una cadena de texto vacía*/
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    console.log('VALUE TO SEARCH', value);
    const results = this.data.vehiculos.filter((item) => {
      return item.placa.toLowerCase().startsWith(value);
    });
    console.log('RESULTS SEARCHED', results);
    if (results.length > 0) {
      results.forEach((item) => {
        console.log('RESULTS VALUE FOREACH', item);
        alert.addInput({
          type: 'radio',
          label: item.placa,
          value: item,
          checked: false
        });
      });

      alert.addButton({
        text: 'Ok',
        handler: data => {
          console.log('Radio data:', data);
          this.entryForm.get('placa').setValue(data);
        }
      });
    } else {
      alert.addButton({
        text: 'Agregar',
        handler: () => {
          this.navCtrl.push(AddPlacaPage, {
            placa: this.entryForm.get('placa').value,
            callback: (_param) => {
              return new Promise(resolve => {
                this.entryForm.get('placa').setValue({
                  id: _param.v_id,
                  placa: _param.v_placa,
                  tipos_vehiculo_id: _param.v_tipos_vehiculo_id,
                  conductor: {
                    id: _param.c_id,
                    dni: _param.c_dni,
                    nombre: _param.c_nombre,
                  },
                  vc: {
                    id: _param.vc_id,
                    vehiculos_id: _param.v_id,
                    conductores_id: _param.c_id,
                    fecha: _param.vc_fecha,
                    estado: _param.vc_estado,
                  }
                });
                resolve();
              })
            },
          });
        }
      });
      alert.setMessage('Placas no encontradas');
    }
    alert.addButton('Cancel');

    alert.present().then();
  }

  // FIN PLACA ///////////////////////////////////////////////////////////////////////////

  // ORIGEN //////////////////////////////////////////////////////////////////////////////////
  doOrigen() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Origen');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.origenField.value) ?
        ( // Es ub objeto?
          _.isObject(this.origenField.value) ?
            this.origenField.value.nombre : // de ser así obtengo su nombre
            this.origenField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.minas.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Minas no encontradas');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.entryForm.get('origen').setValue(data);
      }
    });

    alert.present().then(() => {
    });
  }

  // FIN ORIGEN ///////////////////////////////////////////////////////////////////////////

  // MANTO //////////////////////////////////////////////////////////////////////////////////
  doManto() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Manto');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.mantleField.value) ?
        ( // Es ub objeto?
          _.isObject(this.mantleField.value) ?
            this.mantleField.value.nombre : // de ser así obtengo su nombre
            this.mantleField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.mantos.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Mantos no encontrados');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.entryForm.get('manto').setValue(data);
      }
    });

    alert.present().then(() => {
    });
  }

  // FIN MANTO ///////////////////////////////////////////////////////////////////////////

  // PILAS //////////////////////////////////////////////////////////////////////////////////
  doPila() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Pila');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.pileField.value) ?
        ( // Es ub objeto?
          _.isObject(this.pileField.value) ?
            this.pileField.value.nombre : // de ser así obtengo su nombre
            this.pileField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.pilas.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Pilas no encontradas');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.entryForm.get('pila').setValue(data);
      }
    });

    alert.present().then(() => {
    });
  }

  // FIN PILAS ///////////////////////////////////////////////////////////////////////////

  // PATIOS //////////////////////////////////////////////////////////////////////////////////
  doPatios() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Patios');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.areasField.value) ?
        ( // Es ub objeto?
          _.isObject(this.areasField.value) ?
            this.areasField.value.nombre : // de ser así obtengo su nombre
            this.areasField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.patios.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Patios no encontrados');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.entryForm.get('patio').setValue(data);
      }
    });

    alert.present().then(() => {
    });
  }

  doToast(message: string) {
    const toast = this.toastCtrl.create({
      position: 'top',
      dismissOnPageChange: true,
      duration: 1500,
      message,
      showCloseButton: true,
    });
    toast.present();
  }

  // FIN PATIOS ///////////////////////////////////////////////////////////////////////////

  // CONDUCTORES /////////////////////////////////////////////////////////////////////////////
  async doConductores() {
    // console.log('PRODUCTOS', this.producto);
    let alert = this.alertCtrl.create();
    alert.setTitle('Dni');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.dniField.value) ?
        ( // Es ub objeto?
          _.isObject(this.dniField.value) ?
            this.dniField.value.dni : // de ser así obtengo su nombre
            this.dniField.value // de lo contrario debe ser una string
        ) : '' /* en caso de contener un valor falso se guarda una cadena de texto vacía*/
    ).toUpperCase(); // Y por ultimo se convierte a minusculas.
    console.log('VALUE TO SEARCH', value);
    const results = await this.conductores.findByDni(value);
    console.log('RESULTS TO SEARCH', results);
    if (results.length > 0) {
      results.forEach((item) => {
        console.log('RESULTS VALUE FOREACH', value);
        alert.addInput({
          type: 'radio',
          label: `(${item.dni}) ${item.nombre}`,
          value: item,
          checked: false
        });
      });
    } else alert.setMessage('Dni no encontrado');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.dniField.setValue(data);
      }
    });

    await alert.present();
  }

// FIN CONDUCTORES //////////////////////////////////////////////////////////////////////////
  object(controlName: string) {
    return _.isObject(this.entryForm.get(controlName).value);
  }

  clearControl(controlName) {
    this.entryForm.get(controlName).reset();
  }
}
