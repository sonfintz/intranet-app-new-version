import {ChangeDetectorRef, Component} from '@angular/core';
import {Alert, AlertController, Events, NavController} from 'ionic-angular';
import {AuthService} from "../../providers/_core/auth-service/auth-service";
import {EntradaPage} from "../entrada/entrada";
import {SalidaPage} from "../salida/salida";
import {LoginPage} from "../login/login";
import {Entrada_bascula} from "../../providers/database/entrada_bascula";
import {momentEs} from "../../helpers/date.helper";
import {Salidas_bascula} from "../../providers/database/salidas_bascula";
import {NetworkProvider} from "../../providers/network/network";
import * as _ from "lodash";
import {MezclasPage} from "../mezclas/mezclas";
import {PilasDatabase} from "../../providers/database/pilas.database";
import {PatiosDatabase} from "../../providers/database/patios.database";

@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {

  items;
  items2;
  active: 'entradas' | 'salidas' = 'entradas';
  entries = [];
  outputs = [];
  logoutAlert: Alert;


  get color(){
    return Boolean(NetworkProvider.onLine) ? 'intranet' : "black";
  }
  constructor(public navCtrl: NavController,
              private auth: AuthService,
              private entradasDB: Entrada_bascula,
              private pilas:PilasDatabase,
              private salidasDB: Salidas_bascula,
              private patios: PatiosDatabase,
              private cd: ChangeDetectorRef,
              // private sync: SyncProvider,
              private alertCtrl: AlertController,
              private events: Events) {
    this.initializeItems();
  }

  initializeItems() {
    this.items = this.entries;
    this.items2 = this.outputs;
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      console.log(val);
      this.items = this.entries.filter((data) => {
        return (data.orden.toLowerCase().indexOf(val.toLowerCase()) > -1)
      });
      console.log('EL FILTRO', this.items);
      // this.items = this.postulaciones.filter((item) => {
      //   return (item.origen.toLowerCase().indexOf(val.toLowerCase()) > -1);
      // })
    }
  }

  getItems2(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      console.log(val);
      this.items2 = this.outputs.filter((data) => {
        return (data.orden.toLowerCase().indexOf(val.toLowerCase()) > -1)
      });
      console.log('EL FILTRO', this.items2);
      // this.items = this.postulaciones.filter((item) => {
      //   return (item.origen.toLowerCase().indexOf(val.toLowerCase()) > -1);
      // })
    }
  }

  async loadData() {
    try {
      this.entries = (await this.entradasDB.findAll()).map((entry) => {
        Object.keys(entry).forEach((key: string) => {
          entry[key]= _.isString(entry[key]) ? entry[key].toUpperCase() : entry[key];
        });
        entry.fecha = momentEs(entry.fecha, 'MMM DD, h:m a');
        return entry;
      });
      this.outputs = (await this.salidasDB.findAll()).map((entry) => {
        Object.keys(entry).forEach((key: string) => {
          entry[key]= _.isString(entry[key]) ? entry[key].toUpperCase() : entry[key];
        });
        entry.fecha = momentEs(entry.fecha, 'MMM DD, h:m a');
        entry.observacion = entry.observacion || 'Sin observación';
        return entry;
      });
      this.cd.detectChanges();
    } catch (e) {
      console.error('ERROR EN CONSULTA LOAD DATA', e);
    }
  }

  async ionViewDidLoad() {
    this.active = 'entradas';
    await this.loadData();
    this.cd.detectChanges();
  }

  async mezclaClicked() {
    const callback = () => {
      return new Promise(async resolve => {
        await this.loadData();
        this.events.publish('creado');
        resolve();
      });
    };

    await this.navCtrl.push(MezclasPage, {
      callback
    })
  }

  entradaClicked() {
    const callback = (_params?) => {
      return new Promise(async resolve => {
        await this.loadData();
        this.events.publish('creado');
        resolve();
      });
    };

    this.navCtrl.push(EntradaPage, {
      callback
    })
  }

  salidaClicked() {
    const callback = (_params?) => {
      return new Promise(async resolve => {
        await this.loadData();
        this.events.publish('creado');
        resolve();
      });
    };

    this.navCtrl.push(SalidaPage, {
      callback
    })
  }

  logoutClicked() {
    this.logoutAlert = this.alertCtrl.create
    ({
      title: 'Atención',
      message: '¿Desea Cerrar Sesion?',
      buttons: [
        {
          text: 'Cancelar',
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.auth.logout().subscribe(() => {
              this.navCtrl.setRoot(LoginPage);
            });
          }
        }
      ]
    });
    this.logoutAlert.present();
  }

  doEdit(Obj:any){
  const callback = (_params?) => {
      return new Promise(async resolve => {
        await this.loadData();
        resolve();
      });
    };
    this.navCtrl.push(EntradaPage, {
      callback,
      edit: Obj
    })
  }

  doEditS(Obj:any){
    const callback = (_params?) => {
      return new Promise(async resolve => {
        await this.loadData();
        resolve();
      });
    };
    this.navCtrl.push(SalidaPage, {
      callback,
      edit: Obj
    })
  }

  async test() {
    await this.loadData();
    console.log('LOG', this.outputs,this.entries,);
    // console.log('ENTRADAS ACTUALIZAR', await this.sync.entrada.forUpload());
    // console.log('SALIDAS ACTUALIZAR', await this.sync.salida.forUpload());
  }

  // testClicked(){
  //   this.pilas.findAll().then((data) => {
  //     console.log('pilas',data)
  //   });
  // }
  // testClicked1(){
  //   this.patios.findAll().then((data) => {
  //     console.log('patios',data)
  //   });
  // }

}
