import {Component} from '@angular/core';
import {Alert, AlertController, Loading, LoadingController, NavController, Platform} from 'ionic-angular';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../providers/_core/auth-service/auth-service";
import 'rxjs/add/operator/debounceTime';
import {MainPage} from "../main/main";
import {NetworkProvider} from "../../providers/network/network";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  loading: Loading;

  get onLine() {
    return NetworkProvider.onLine;
  }

  claveTitle = 'Clave';

  errorAlert: Alert;

  constructor(public navCtrl: NavController,
              private fb: FormBuilder,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private auth: AuthService,
              private platform: Platform,
  ) {
    this.platform.ready().then(() => {
      this.verificacionUser();
    })
  }

  get color(){
    return Boolean(NetworkProvider.onLine) ? "primary" : "secondary";
  }

  ionViewWillLoad() {

    this.loginForm = this.fb.group({
      correo: new FormControl(null, Validators.compose([Validators.required, Validators.email])),
      clave: new FormControl(null, Validators.compose([Validators.minLength(6), Validators.required])),
    });
    this.loginForm.valueChanges
      .debounceTime(400)
      .subscribe(data => this.onValueChanged(data));
  }

  formErrors = {
    'correo': [],
    'clave': [],
  };

  validationMessages = {
    'correo': {
      'required': 'Correo es requerido',
      'pattern': 'Ingrese un Email valido.'
    },
    'clave': {
      'required': 'Clave es requerido',
      'minlength': 'El minimo son 6 caracteres'
    },
  };

  onValueChanged(data?: any) {
    if (!this.loginForm) {
      return;
    }
    const form = this.loginForm;
    for (const field in this.formErrors) {
      // Limpiamos los mensajes anteriores
      this.formErrors[field] = [];
      this.loginForm[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Autenticando, por favor espere un momento...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  public login() {
    this.showLoading();
    const form = this.loginForm.value;
    const data = {correo: form.correo};
    const key = Boolean(this.onLine) ? 'clave' : 'pin';
    data[key] = form.clave;
    console.log('LOGIN', this.onLine, data);
    let flag = false;
    console.warn('LOGIN 1');
    this.auth.login(data).subscribe(
      () => {
        this.loading.dismiss();
        console.warn('LOGIN 2');
        flag = true;
      },
      error => {
        this.loading.dismiss();
        this.errorAlert = this.alertCtrl.create
        ({
          title: 'Denegado',
          subTitle: 'Usuario y/o Contraseña incorrectas',
          buttons: ['Ok']
        });
        this.errorAlert.present();
        flag = false;
        console.log('ERROR LOGIN', error);
      },
      () => {
        if (flag) {
          console.warn('LOGIN 4');
          if (flag) {
            this.navCtrl.setRoot(MainPage);
          }
        }
      });
    console.warn('LOGIN 5');
  }

  showError(text) {
    this.errorAlert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    this.errorAlert.present();
  }

  verificacionUser() {
    if (AuthService.isAuthenticated) {
      this.navCtrl.setRoot(MainPage);
    }
  }
}
