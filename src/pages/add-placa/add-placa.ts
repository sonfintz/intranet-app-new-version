import {Component} from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Tipos_vehiculosDatabase} from "../../providers/database/tipos_vehiculos.database";
import {VehiculosDatabase} from "../../providers/database/vehiculos.database";
import {Vehiculos_conductoresDatabase} from "../../providers/database/vehiculos_conductores.database";
import * as _ from "lodash";
import {ConductoresDatabase} from "../../providers/database/conductores.database";
import {NetworkProvider} from "../../providers/network/network";

/**
 * Generated class for the AddPlacaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-placa',
  templateUrl: 'add-placa.html',
})
export class AddPlacaPage {
  form: FormGroup;
  data: any = {
    tipos_vehiculos: [],
    conductores: [],
  };
  callback: (param: any) => Promise<void>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public tipo_vehiculo: Tipos_vehiculosDatabase,
              public placa: VehiculosDatabase,
              public alertCtrl: AlertController,
              public conductores: ConductoresDatabase,
              public vehiculos_conductores: Vehiculos_conductoresDatabase
  ) {
    this.createMyForm();
    this.callback = this.navParams.get('callback');
  }

  get color(){
    return Boolean(NetworkProvider.onLine) ? "intranet" : "black";
  }

  async ionViewDidLoad() {
    await this.placa.isReady();
    this.data.tipos_vehiculos = await this.tipo_vehiculo.findAll();
    this.data.conductores = await this.conductores.findAll();
    console.log('ionViewDidLoad', this.data);
  }

  private createMyForm() {
    this.form = this.formBuilder.group({
      tipos_vehiculos: new FormControl(null, Validators.compose([
        Validators.required,
      ])),
      placa: new FormControl(this.navParams.get('placa'), Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6)
      ])),
      dni: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])),
      conductor: new FormControl(null, Validators.compose([
        Validators.required,
      ])),
    });
    this.form.get('dni').valueChanges.subscribe((value) => {
      if (_.isObject(value)) {
        this.form.get('conductor').setValue(value.nombre);
        this.form.get('conductor').disable();
      }else {
        this.form.get('conductor').reset();
        this.form.get('conductor').enable();
      }
    });
    this.form.valueChanges
      .debounceTime(400)
      .subscribe(data => this.onValueChanged(data));
  }

  formErrors = {
    'dni': [],
    'placa': []
  };

  validationMessages = {
    'dni': {
      'minlength': 'El minimo debe ser de 6 digitos'
    },
    'placa': {
      'minlength': 'El minimo debe ser de 6 digitos'
    }
  };

  onValueChanged(data?: any) {
    if (!this.form) {
      return;
    }
    const form = this.form;
    for (const field in this.formErrors) {
      // Limpiamos los mensajes anteriores
      this.formErrors[field] = [];
      this.form[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  async saveData() {
    try {
      if (this.form.valid) {
        const form = this.form.value;
        const body = {
          vehiculo: {
            placa: form.placa,
            tipos_vehiculo: form.tipos_vehiculos.id,
          },
          conductor: _.isObject(form.dni) ? {id: form.dni.id} : {
            dni: form.dni,
            nombre: form.conductor
          }
        };
        let result = await this.vehiculos_conductores.save(body);
        if(this.callback) {
          this.callback(result).then(() => this.navCtrl.pop());
        }
      }
    } catch (e) {
      console.error("[AddPlacaPage] ERROR AL GUARDAR EL FORMULARIO", e);
    }
  }

  get dniField() {
    return this.form.get('dni');
  }

  get tipoVehiculoField(){
    return this.form.get('tipos_vehiculos')
  }

// TIPOS DE VEHICULOS
  doTiposVehiculo() {
    // console.log('PRODUCTOS', this.producto);
    let alert = this.alertCtrl.create();
    alert.setTitle('Tipo Vehiculo');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.tipoVehiculoField.value) ?
        ( // Es ub objeto?
          _.isObject(this.tipoVehiculoField.value) ?
            this.tipoVehiculoField.value.nombre : // de ser así obtengo su nombre
            this.tipoVehiculoField.value // de lo contrario debe ser una string
        ) : '' /* en caso de contener un valor falso se guarda una cadena de texto vacía*/
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    console.log('VALUE TO SEARCH', value);
    const results = this.data.tipos_vehiculos.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    console.log('RESULTS TO SEARCH', results);
    if (results.length > 0) {
      results.forEach((item) => {
        console.log('RESULTS VALUE FOREACH', value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Tipos de Vehiculos no encontrados');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.tipoVehiculoField.setValue(data);
      }
    });
    alert.present();
  }

  // CONDUCTORES /////////////////////////////////////////////////////////////////////////////
  doConductores() {
    // console.log('PRODUCTOS', this.producto);
    let alert = this.alertCtrl.create();
    alert.setTitle('Dni');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.dniField.value) ?
        ( // Es ub objeto?
          _.isObject(this.dniField.value) ?
            this.dniField.value.dni : // de ser así obtengo su nombre
            this.dniField.value // de lo contrario debe ser una string
        ) : '' /* en caso de contener un valor falso se guarda una cadena de texto vacía*/
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    console.log('VALUE TO SEARCH', value);
    const results = this.data.conductores.filter((item) => {
      return item.dni.toLowerCase().startsWith(value);
    });
    console.log('RESULTS TO SEARCH', results);
    if (results.length > 0) {
      results.forEach((item) => {
        console.log('RESULTS VALUE FOREACH', value);
        alert.addInput({
          type: 'radio',
          label: `(${item.dni}) ${item.nombre}`,
          value: item,
          checked: false
        });
      });
    } else alert.setMessage('Dni no encontrado');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.dniField.setValue(data);
      }
    });

    alert.present().then();
  }

  object(controlName: string) {
    return _.isObject(this.form.get(controlName).value);
  }

  clearControl(controlName) {
    this.form.get(controlName).reset();
  }
}
