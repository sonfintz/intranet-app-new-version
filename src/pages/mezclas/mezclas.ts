import {Component} from '@angular/core';
import {AlertController, NavController, NavParams, ToastController} from 'ionic-angular';
import {NetworkProvider} from "../../providers/network/network";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import * as _ from "lodash";
import {MinasDatabase} from "../../providers/database/minas.database";
import {datetime} from "../../helpers/date.helper";
import {IMezcla} from "../../models/mezcla";
import {Mezcla_bascula} from "../../providers/database/mezcla_bascula";
import {PatiosDatabase} from "../../providers/database/patios.database";

/**
 * Generated class for the MezclasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mezclas',
  templateUrl: 'mezclas.html',
})
export class MezclasPage {
  public opc : number = 0;
  mezclaForm: FormGroup;
  data: any = {
    patios: [],
    minas: []
  };
  parentCallback;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public origen: MinasDatabase,
              public areas: PatiosDatabase,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public mezcla: Mezcla_bascula,
              public formBuilder: FormBuilder) {
    Promise.all([
      this.origen.findAll(),
      this.areas.findAll(),
    ])
      .then((select: any[]) => {
        console.log('MEZCLAS RESULTS', select);
        this.data.minas = select[0];
        this.data.patios = select[1];
      })
      .catch((e) => {
        console.log('CATCH', e);
      });
    this.createMyForm();
  }

  saveData(){
    const form = this.mezclaForm.value;
    console.log('SARAMAMBICHE', form);
    const body: IMezcla = {
      recibo: form.recibo,
      fecha: datetime(),
      patios_id: form.patio.id,
      observacion: form.observacion,
      capacidad_cargador: form.capacidad,
      numero_orden: form.orden,
      total_rondas: form.total_rondas,
      tiempo_cargador: form.tiempo,
      total_tonelada_mezcla: form.mezcla,
    };
    console.log('BODY', form);
    this.mezcla.save(body).then(
      (data) => {
        const alert = this.toastCtrl.create({
          message: 'MEZCLA Almacenada',
          position: 'bottom',
          duration: 2000,
          showCloseButton: true
        });
        alert.present();
        console.log('MEZCLA SAVED', data);
        this.parentCallback(data).then(() => this.navCtrl.pop());
      },
      error => {
        console.log('ERROR ON SAVE MEZCLA', error);
      }
    );
  }

  private createMyForm(){
    this.mezclaForm = this.formBuilder.group({
      recibo: [null, Validators.compose([
        Validators.required,
      ])],
      origen: [null, Validators.compose([
        Validators.required,
      ])],
      capacidad: [null, Validators.compose([
        Validators.required,
      ])],
      tiempo: [null, Validators.compose([
        Validators.required,
      ])],
      rondas: [null, Validators.compose([
        Validators.required,
      ])],
      orden: [null, Validators.compose([
        Validators.required,
      ])],
      destino: [null, Validators.compose([
        Validators.required,
      ])],
      mezcla: [null, Validators.compose([
        Validators.required,
      ])],
      patio: [null, Validators.compose([
        Validators.required,
      ])],
      observacion: [null, Validators.compose([
      ])],
    })
  }

  get origenField() {
    return this.mezclaForm.get('origen');
  }

  get areasField() {
    return this.mezclaForm.get('patio');
  }

  get color() {
    return Boolean(NetworkProvider.onLine) ? "intranet" : "black";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MezclasPage');
  }

  exit(a: number){
    if (this.opc != 0){
      this.opc = this.opc - a;
    }
    console.log('SALIENDO', this.opc)
  }

  next(a: number){
    if (this.opc != 1){
      this.opc = this.opc + a;
    }
    console.log('NEXT', this.opc)
  }

  object(controlName: string) {
    return _.isObject(this.mezclaForm.get(controlName).value);
  }

  clearControl(controlName) {
    this.mezclaForm.get(controlName).reset();
  }

  // ORIGEN //////////////////////////////////////////////////////////////////////////////////
  doOrigen() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Origen');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.origenField.value) ?
        ( // Es ub objeto?
          _.isObject(this.origenField.value) ?
            this.origenField.value.nombre : // de ser así obtengo su nombre
            this.origenField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.minas.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Minas no encontradas');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.mezclaForm.get('origen').setValue(data);
      }
    });

    alert.present().then(() => {
    });
  }

  // FIN ORIGEN ///////////////////////////////////////////////////////////////////////////

  // PATIOS //////////////////////////////////////////////////////////////////////////////////
  doPatios() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Patios');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.areasField.value) ?
        ( // Es ub objeto?
          _.isObject(this.areasField.value) ?
            this.areasField.value.nombre : // de ser así obtengo su nombre
            this.areasField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.patios.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Patios no encontrados');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.mezclaForm.get('patio').setValue(data);
      }
    });

    alert.present().then(() => {
    });
  }
}
