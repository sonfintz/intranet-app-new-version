import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {AlertController, Events, NavController, NavParams, TextInput, ToastController} from 'ionic-angular';
import {AddPlacaPage} from "../add-placa/add-placa";
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from "@angular/forms";
import * as _ from 'lodash';
import {MaterialesDatabase} from "../../providers/database/materiales.database";
import {MinasDatabase} from "../../providers/database/minas.database";
import {ClientesDatabase} from "../../providers/database/clientes.database";
import {VehiculosDatabase} from "../../providers/database/vehiculos.database";
import {Vehiculos_conductoresDatabase} from "../../providers/database/vehiculos_conductores.database";
import {Salidas_bascula} from "../../providers/database/salidas_bascula";
import {SQLite} from "@ionic-native/sqlite";
import {NetworkProvider} from "../../providers/network/network";
import {ConductoresDatabase} from "../../providers/database/conductores.database";
import {PatiosDatabase} from "../../providers/database/patios.database";
import {PilasDatabase} from "../../providers/database/pilas.database";
import * as moment from "moment";
import {IOut} from "../../models/out";
import {datetime} from "../../helpers/date.helper";

export function weight(msg: string = 'El peso bruto debe ser mayor que el peso tara.'): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if(!Boolean(control.get('tara').value)){
      return null;
    }
    const gross = Number(control.get('bruto').value);
    const tare = Number(control.get('tara').value);
    return Boolean(gross <= tare) ? {'weight': msg} : null;
  };
}

/**
 * Generated class for the SalidaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-salida',
  templateUrl: 'salida.html',
})

export class SalidaPage implements OnInit {
  /*
  DECLARACIONES
   */
  edit: boolean;
  outForm: FormGroup;
  data: any = {
    vehiculos: [],
    materiales: [],
    minas: [],
    patios: [],
    pilas: [],
    clientes: [],
    conductores: []
  };
  testRadioOpen: boolean;
  testRadioResult;
  dni: any[] = [];
  placa: any[] = [];
  parentCallback;
  fecha = moment();
  hora = moment();

  @ViewChild('iTara') taraField: TextInput;

  constructor(public navCtrl: NavController,
              public formBuilder: FormBuilder,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public material: MaterialesDatabase,
              public mines: MinasDatabase,
              public areas: PatiosDatabase,
              public pilas: PilasDatabase,
              public clients: ClientesDatabase,
              public conductores: ConductoresDatabase,
              public sqlite: SQLite,
              public vehiculo: VehiculosDatabase,
              public vehiculos_conductores: Vehiculos_conductoresDatabase,
              public salida: Salidas_bascula,
              public events: Events,
              public toastCtrl: ToastController,
              public cd: ChangeDetectorRef
  ) {
    Promise.all([
      this.vehiculo.findAll(),
      this.material.findAll(),
      this.mines.findAll(),
      this.clients.findAll(),
      this.areas.findAll(),
    ])
      .then((select: any[]) => {
        this.data.vehiculos = select[0];
        this.data.materiales = select[1];
        this.data.minas = select[2];
        this.data.clientes = select[3];
        this.data.patios = select[4];
      })
      .catch((e) => {
        console.log('CATCH', e);
      });
    this.createMyForm();

    this.parentCallback = this.navParams.get('callback');

  }

  timeChange($event){
    console.log($event);
    this.hora = moment($event);
  }

  dateChange($event){
    console.log($event);
    this.fecha = moment($event);
  }

  checked(){
    this.outForm.get('transportadora').setValue(!(this.outForm.get('transportadora').value));
  }

  get color(){
    return Boolean(NetworkProvider.onLine) ? "intranet" : "black";
  }

  ionViewDidLoad() {
    const toEdit = this.navParams.get('edit');
    if (Boolean(toEdit)){
      this.outForm.get('id').setValue(toEdit.id);
      this.placaField.setValue(toEdit.vehiculo_placa);
      this.dniField.setValue(toEdit.conductor_dni);
      this.materialField.setValue(toEdit.material_nombre);
      this.origenField.setValue(toEdit.minas_id);
      this.clientField.setValue(toEdit.clientes_nombre);
      this.reciboField.setValue(toEdit.recibo);
      this.brutoField.setValue(toEdit.bruto);
      this.taraFields.setValue(toEdit.tara);
      this.patiosField.setValue(toEdit.patio_nombre);
      this.pileField.setValue(toEdit.pila_nombre);
    } else {
      const x: Array<string> = ['materiales', 'vehiculos', 'clientes', 'minas', 'patios', 'pilas',];
      this.events.subscribe('new-data', async (data) => {
        if(x.includes(data)) {
          switch (data) {
            case 'materiales':
              this.data.materiales = await this.material.findAll();
              break;
            case 'vehiculos':
              this.data.vehiculos = await this.vehiculo.findAll();
              break;
            case 'clientes':
              this.data.clientes = await this.clients.findAll();
              break;
            case 'minas':
              this.data.minas = await this.mines.findAll();
              break;
            case 'patios':
              this.data.patios = await this.areas.findAll();
              break;
            case 'pilas':
              this.data.pilas = await this.pilas.findAll();
              break;
          }
          console.log('subscribed-on-new-data', data, this.data);
        }
        this.doToast(`Actualizando datos`);
        // this.doToast2(`Actualizando datos`);
      });
    }
  }

  ionViewDidEnter() {
    console.log('BEfore focus');
    if (this.edit) {
      setTimeout(() => {
        this.taraField.setFocus();
        console.log('FOCUS', this.taraField);
      }, 600);
    }
  }

  dateParse() {
    return this.fecha
      .add(this.hora.hour(), 'hours')
      .add(this.hora.minute(), 'minutes')
      .format('YYYY-MM-DD HH:mm:ss');
  }

  saveData() {
    if (this.outForm.valid) {
      const form = this.outForm.value;
      form.fecha_registro = this.dateParse();
      if (this.edit) {
        console.log(this.edit);
        this.salida.update(form.id, form.tara, form.bruto).then(
          (data) => {
            const alert = this.toastCtrl.create({
              message:'Salida Actualizada',
              position: 'bottom',
              duration: 2000,
              showCloseButton: true
            });
            this.cd.detectChanges();
            alert.present();
            console.log('SALIDA UPDATED', data);
            this.parentCallback(data).then(() => this.navCtrl.pop());
          },
          error => {
            console.log('ERROR ON UPDATED OUTPUT', error);
          });
      } else {
        console.log('FORM', form);
        const body: IOut = {
          recibo: form.recibo,
          bruto: form.bruto,
          neto: form.neto,
          tara: form.tara,
          observacion: form.observacion,
          conductor: {
            id: form.dni.id,
            dni: form.dni.dni,
            nombre: form.dni.nombre
          },
          vehiculo: {
            id: form.placa.id,
            placa: form.placa.placa,
            tipos_vehiculo_id: form.placa.tipos_vehiculo_id
          },
          fecha: datetime(),
          fecha_registro: form.fecha_registro,
          transportadora: form.transportadora === 'true' ? 1 : 0,
          patios_id: form.patio.id,
          pilas_id: form.pila.id,
          materiales_id: form.material.id,
          clientes_id: form.cliente.id,
          minas_id : form.origen.id
        };
        console.log('CUERPO SALIDA', body);
        this.salida.save(body).then(
          (data) => {
            const alert = this.toastCtrl.create({
              message:'Salida Almacenada',
              position: 'bottom',
              duration: 2000,
              showCloseButton: true
            });
            alert.present();
            console.log('SALIDA SAVED', data);
            this.parentCallback(data).then(() => this.navCtrl.pop());
          },
          error => {
            const alert = this.toastCtrl.create({
              message: error,
              position: 'top',
              duration: 10000,
              showCloseButton: true
            });
            alert.present();
            console.error('ERROR ON SAVE OUTPUT', error);
          }
        );
      }
    }
  }

  private createMyForm() {
    this.outForm = this.formBuilder.group({
      recibo: new FormControl(null, Validators.compose([
        Validators.required,
      ])),
      placa: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6)
      ])),
      dni: new FormControl(null, Validators.compose([
        Validators.required,
      ])),
      material: new FormControl(null, Validators.compose([
        Validators.required,
      ])),
      bruto: [null, Validators.compose([
        Validators.required, Validators.min(0),
        Validators.max(75)
      ])],
      tara: [null, Validators.compose([Validators.min(0)])],
      neto: [null, Validators.compose([Validators.min(1)])],
      origen: new FormControl(null, Validators.compose([
        Validators.required,
      ])),
      patio: [null, Validators.compose([
        Validators.required,
      ])],
      pila: [null, Validators.compose([
        Validators.required,
      ])],
      cliente: new FormControl(null, Validators.compose([
        Validators.required,])),
      transportadora: false,
      fecha_registro: null,
      hora_registro: null,
      observacion: new FormControl(null),
      id: new FormControl(null)
    }, {
      validator: weight()
    });
    this.outForm.get('patio').valueChanges
      .subscribe((value: any) => {
        if (_.isObject(value)) {
          this.pilas.findByField('patios_id', value.id)
            .then((data) => {
              console.log('LLEGO', data);
              this.data.pilas = data;
            }).catch((data) => {
            console.error('No llego', data);
          });
        }
      });
    this.outForm.get('placa').valueChanges.subscribe((value: any) => {
      if (_.isObject(value)) {
        console.log('INNER', value);
        this.vehiculos_conductores.getDriver(value.id)
          .then((r) => {
            this.outForm.get('dni').setValue(r);
          }).catch((r) => {
          console.log('error Drivers', r);
        });
      } else {
        this.outForm.get('dni').setValue(null);
      }
    });
    this.outForm.valueChanges
      .debounceTime(400)
      .subscribe(data => this.onValueChanged(data));

    this.outForm.get('id').valueChanges.subscribe((v) => {
      this.edit = Boolean(v);
    });
  }

  formErrors = {
    'bruto': [],
    'placa': []
  };

  validationMessages = {
    'bruto': {
      'max': 'Maximo 75 Toneladas',
    },
    'placa': {
      'minlength': 'Minimo 6 digitos'
    },
  };

  onValueChanged(data?: any) {
    if (!this.outForm) {
      return;
    }
    const form = this.outForm;
    for (const field in this.formErrors) {
      // Limpiamos los mensajes anteriores
      this.formErrors[field] = [];
      this.outForm[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field].push(messages[key]);
        }
      }
    }
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.outForm.get('bruto').valueChanges.subscribe((val) => {
      if (isNaN(val)) {
        this.outForm.get('bruto').setErrors({nan: 'no es un número'});
      }
      const neto = val - this.outForm.get('tara').value;
      if (neto > 0) {
        this.outForm.get('neto').setValue(neto);
      } else {
        this.outForm.get('neto').setErrors({invalidNet: 'peso neto debe ser positivo.'});
      }
    });
    this.outForm.get('tara').valueChanges.subscribe((val) => {
      if (isNaN(val)) {
        this.outForm.get('tara').setErrors({nan: 'no es un número'});
      }
      const neto = this.outForm.get('bruto').value - val;
      if (neto > 0) {
        this.outForm.get('neto').setValue(neto);
      } else {
        this.outForm.get('neto').setErrors({invalidNet: 'peso neto debe ser positivo.'});
      }
    });
    this.outForm.get('bruto').valueChanges.subscribe((val) => {
      const bruto = this.outForm.get('bruto').value;
      if (bruto <= 0) {
        this.outForm.get('neto').setValue(0);
      }
    });
    this.outForm.get('tara').valueChanges.subscribe((val) => {
      const tara = this.outForm.get('tara').value;
      if (tara <= 0) {
        this.outForm.get('neto').setValue(0);
      }
    });
  }

  get brutoField(){
    return this.outForm.get('bruto');
  }

  get taraFields(){
    return this.outForm.get('tara');
  }

  get dniField() {
    return this.outForm.get('dni');
  }

  get reciboField(){
    return this.outForm.get('recibo');
  }

  get materialField() {
    return this.outForm.get('material');
  }

  get placaField() {
    return this.outForm.get('placa');
  }

  get origenField() {
    return this.outForm.get('origen');
  }

  get areasField() {
    return this.outForm.get('patio');
  }

  get patiosField() {
    return this.outForm.get('patio')
  }

  get pileField() {
    return this.outForm.get('pila');
  }

  get clientField() {
    return this.outForm.get('cliente')
  }

  get netWeight() {
    return this.outForm.get('neto');
  }

  //PLACA ///////////////////////////////////////////////////////////////////////////////
  doPlaca() {
    // console.log('PLACA', this.placa);
    let alert = this.alertCtrl.create({
      title: 'Vehículos'
    });
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.placaField.value) ?
        ( // Es ub objeto?
          _.isObject(this.placaField.value) ?
            this.placaField.value.placa : // de ser así obtengo su nombre
            this.placaField.value // de lo contrario debe ser una string
        ) : '' /* en caso de contener un valor falso se guarda una cadena de texto vacía*/
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.vehiculos.filter((item) => {
      return item.placa.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log('RESULTS VALUE FOREACH', item);
        alert.addInput({
          type: 'radio',
          label: item.placa,
          value: item,
          checked: false
        });
      });

      alert.addButton({
        text: 'Ok',
        handler: data => {
          console.log('Radio data:', data);
          this.outForm.get('placa').setValue(data);
        }
      });
    } else {
      alert.addButton({
        text: 'Agregar',
        handler: () => {
          this.navCtrl.push(AddPlacaPage, {
            placa: this.outForm.get('placa').value,
            callback: (_param) => {
              return new Promise(resolve => {
                this.placaField.setValue({
                  id: _param.v_id,
                  placa: _param.v_placa,
                  tipos_vehiculo_id: _param.v_tipos_vehiculo_id,
                  conductor: {
                    id: _param.c_id,
                    dni: _param.c_dni,
                    nombre: _param.c_nombre,
                  },
                  vc: {
                    id: _param.vc_id,
                    vehiculos_id: _param.v_id,
                    conductores_id: _param.c_id,
                    fecha: _param.vc_fecha,
                    estado: _param.vc_estado,
                  }
                });
                resolve();
              })
            },
          });
        }
      });
      alert.setMessage('Placas no encontradas');
    }
    alert.addButton('Cancel');

    alert.present().then();
  }

  // FIN PLACA ///////////////////////////////////////////////////////////////////////////

  // MATERIAL //////////////////////////////////////////////////////////////////////////////////
  doMaterial() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Material');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.materialField.value) ?
        ( // Es ub objeto?
          _.isObject(this.materialField.value) ?
            this.materialField.value.nombre : // de ser así obtengo su nombre
            this.materialField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.materiales.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Material no encontrado');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.outForm.get('material').setValue(data);
        this.testRadioOpen = false;
        this.testRadioResult = data;
      }
    });

    alert.present().then(() => {
      this.testRadioOpen = true;
    });
  }

  // FIN MATERIAL //////////////////////////////////////////////////////////////////////////////

  // ORIGEN //////////////////////////////////////////////////////////////////////////////////
  doOrigen() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Origen');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.origenField.value) ?
        ( // Es ub objeto?
          _.isObject(this.origenField.value) ?
            this.origenField.value.nombre : // de ser así obtengo su nombre
            this.origenField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.minas.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Minas no encontradas');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.outForm.get('origen').setValue(data);
        this.testRadioOpen = false;
        this.testRadioResult = data;
      }
    });

    alert.present().then(() => {
      this.testRadioOpen = true;
    });
  }

  // FIN ORIGEN ///////////////////////////////////////////////////////////////////////////

  // ORIGEN //////////////////////////////////////////////////////////////////////////////////
  doCliente() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Cliente');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.clientField.value) ?
        ( // Es ub objeto?
          _.isObject(this.clientField.value) ?
            this.clientField.value.nombre : // de ser así obtengo su nombre
            this.clientField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.clientes.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Clientes no encontrados');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.outForm.get('cliente').setValue(data);
        this.testRadioOpen = false;
        this.testRadioResult = data;
      }
    });

    alert.present().then(() => {
      this.testRadioOpen = true;
    });
  }

  doToast(message: string) {
    const toast = this.toastCtrl.create({
      position: 'top',
      dismissOnPageChange: true,
      duration: 1500,
      message,
      showCloseButton: true,
    });
    toast.present();
  }

  // FIN ORIGEN ///////////////////////////////////////////////////////////////////////////

  // CONDUCTORES /////////////////////////////////////////////////////////////////////////////
  async doConductores() {
    // console.log('PRODUCTOS', this.producto);
    let alert = this.alertCtrl.create();
    alert.setTitle('Dni');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.dniField.value) ?
        ( // Es ub objeto?
          _.isObject(this.dniField.value) ?
            this.dniField.value.dni : // de ser así obtengo su nombre
            this.dniField.value // de lo contrario debe ser una string
        ) : '' /* en caso de contener un valor falso se guarda una cadena de texto vacía*/
    ).toUpperCase(); // Y por ultimo se convierte a minusculas.
    console.log('VALUE TO SEARCH', value);
    const results = await this.conductores.findByDni(value);
    console.log('RESULTS TO SEARCH', results);
    if (results.length > 0) {
      results.forEach((item) => {
        console.log('RESULTS VALUE FOREACH', value);
        alert.addInput({
          type: 'radio',
          label: `(${item.dni}) ${item.nombre}`,
          value: item,
          checked: false
        });
      });
    } else alert.setMessage('Dni no encontrado');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.dniField.setValue(data);
      }
    });

    await alert.present();
  }

// FIN CONDUCTORES //////////////////////////////////////////////////////////////////////////

  // PILAS //////////////////////////////////////////////////////////////////////////////////
  doPila() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Pila');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.pileField.value) ?
        ( // Es ub objeto?
          _.isObject(this.pileField.value) ?
            this.pileField.value.nombre : // de ser así obtengo su nombre
            this.pileField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.pilas.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Pilas no encontradas');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.outForm.get('pila').setValue(data);
      }
    });

    alert.present().then(() => {
    });
  }

  // FIN PILAS ///////////////////////////////////////////////////////////////////////////

  // PATIOS //////////////////////////////////////////////////////////////////////////////////
  doPatios() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Patios');
    const value = (
      // Contiene un valor diferente a null o undefined?
      Boolean(this.areasField.value) ?
        ( // Es ub objeto?
          _.isObject(this.areasField.value) ?
            this.areasField.value.nombre : // de ser así obtengo su nombre
            this.areasField.value // de lo contrario debe ser una string
        ) : '' // en caso de contener un valor falso se guarda una cadena de texto vacía
    ).toLowerCase(); // Y por ultimo se convierte a minusculas.
    const results = this.data.patios.filter((item) => {
      return item.nombre.toLowerCase().startsWith(value);
    });
    if (results.length > 0) {
      results.forEach((item) => {
        console.log(value);
        alert.addInput({
          type: 'radio',
          label: item.nombre,
          value: item,
          checked: false
        });
      });
    } else {
      alert.setMessage('Patios no encontrados');
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.outForm.get('patio').setValue(data);
      }
    });

    alert.present().then(() => {
    });
  }

  // doToast2(message: string) {
  //   const toast = this.toastCtrl.create({
  //     position: 'top',
  //     dismissOnPageChange: true,
  //     duration: 1500,
  //     message,
  //     showCloseButton: true,
  //   });
  //   toast.present();
  // }

  // FIN PATIOS //////////////////////////////////////////////////////////////////

  object(controlName: string) {
    return _.isObject(this.outForm.get(controlName).value);
  }

  clearControl(controlName) {
    this.outForm.get(controlName).reset();
  }
}
