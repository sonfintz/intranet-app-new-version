import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Api} from "../api/api";
import {NetworkProvider} from "../../network/network";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import 'rxjs/add/operator/share';
import {UsersDatabase} from "../../database/users.database";
import {AuthCredentials, IAuthCredentials} from "../../../models/user";
import {fromPromise} from "rxjs/observable/fromPromise";

@Injectable()
export class AuthService {
  onAuthenticate: BehaviorSubject<boolean>;

  constructor(private api: Api,
              private users: UsersDatabase) {
    this.onAuthenticate = new BehaviorSubject<boolean>(false);
    this.onAuthenticate.next(AuthService.isAuthenticated);
  }

  get onAuthenticateEvent() {
    return this.onAuthenticate.asObservable();
  }

  static get isAuthenticated() {
    return Boolean(localStorage.getItem('user-data'));
  }

  static get getBearer() {
    return 'bearer ' + JSON.parse(localStorage.getItem('user-data')).access_token;
  }

  static set currentUser(data: any) {
    localStorage.setItem('user-data', JSON.stringify(data));
  }

  static get currentUser() {
    return JSON.parse(localStorage.getItem('user-data'));
  }

  public login(credentials: IAuthCredentials): Observable<any> {
    credentials = new AuthCredentials(credentials).toObject();
    if (NetworkProvider.onLine) {
      console.log('ONLINE');
      const req = this.api.post("auth/login", credentials).share();
      req.subscribe(
        (data: any) => {
          AuthService.currentUser = data;
          this.onAuthenticate.next(true);
        },
        () => {
          this.onAuthenticate.next(false)
        }
      );
      return req;
    } else {
      console.log('OFFLINE');
      return fromPromise(this.users.authentication(credentials));
    }
  }

  public logout() {
    localStorage.removeItem('user-data');
    return Observable.create(observer => {
      this.onAuthenticate.next(false);
      observer.next(true);
      observer.complete();
    });
  }

  public reconnected() {
    return this.api.post('auth/reconnected', AuthService.currentUser);
  }
}
