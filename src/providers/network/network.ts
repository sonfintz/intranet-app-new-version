import {Injectable} from '@angular/core';

/*
  Generated class for the NetworkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetworkProvider {
  static get onLine() {
    return Boolean(JSON.parse(localStorage.getItem('network')));
  }

  static set onLine(stat: boolean) {

    console.warn('\n\n\n\n\n network \n\n\n\n\n', JSON.stringify(stat));
    localStorage.setItem('network', JSON.stringify(stat));
  }

  // private _onLine: BehaviorSubject<boolean>;
  //
  // private storage: Storage;
  //
  // constructor(public network: Network) {
  //   this.storage = new Storage({
  //     name: 'intranet.db',
  //     storeName: 'global'
  //   });
  //   this._onLine = new BehaviorSubject<boolean>(this.connected);
  //   this.network.onConnect().subscribe(() => this.connected = true);
  //   this.network.onDisconnect().subscribe(() => this.connected = false);
  // }
  //
  // private get connected(): boolean {
  //   let res = Boolean(this.storage.get('network'));
  //   if (!res) {
  //     this.storage.set('network', false);
  //     res = false;
  //   }
  //   return res;
  // }
  //
  // private set connected(val: boolean) {
  //   this._onLine.next(val);
  //   this.storage.set('network', val);
  // }
  //
  // public get onLine() {
  //   return this._onLine.asObservable();
  // }

}
