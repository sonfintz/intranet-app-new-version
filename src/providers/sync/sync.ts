import {Injectable} from '@angular/core';
import {Api} from "../_core/api/api";
import {UsersDatabase} from "../database/users.database";
import {ProductsDatabase} from "../database/products.database";
import {ClientesDatabase} from "../database/clientes.database";
import {MaterialesDatabase} from "../database/materiales.database";
import {Tipos_vehiculosDatabase} from "../database/tipos_vehiculos.database";
import {VehiculosDatabase} from "../database/vehiculos.database";
import {ConductoresDatabase} from "../database/conductores.database";
import {Vehiculos_conductoresDatabase} from "../database/vehiculos_conductores.database";
import {PatiosDatabase} from "../database/patios.database";
import {PilasDatabase} from "../database/pilas.database";
import {MinasDatabase} from "../database/minas.database";
import {MantosDatabase} from "../database/mantos.database";
import {Entrada_bascula} from "../database/entrada_bascula";
import {Salidas_bascula} from "../database/salidas_bascula";
import {DatabaseProvider} from "../database/database";
import {Network} from "@ionic-native/network";
import {delay, distinctUntilChanged, tap} from "rxjs/operators";
import {Events, ToastController} from "ionic-angular";
import {NetworkProvider} from "../network/network";
import {Socket} from "ngx-socket-io";
import * as _ from "lodash";
import {AuthService} from "../_core/auth-service/auth-service";
import {Observable} from "rxjs/Observable";
import {Mezcla_bascula} from "../database/mezcla_bascula";

@Injectable()
export class SyncProvider {

  reconnectRx: Observable<any>;
  onWelcome: Observable<any>;
  onNews: Observable<any>;

  constructor(public api: Api,
              public network: Network,
              public usersDb: UsersDatabase,
              public products: ProductsDatabase,
              public clientes: ClientesDatabase,
              public materiales: MaterialesDatabase,
              public tipos_vehiculos: Tipos_vehiculosDatabase,
              public vehiculos: VehiculosDatabase,
              public conductores: ConductoresDatabase,
              public vehiculo_conductores: Vehiculos_conductoresDatabase,
              public patios: PatiosDatabase,
              public pilas: PilasDatabase,
              public minas: MinasDatabase,
              public mantos: MantosDatabase,
              public entrada: Entrada_bascula,
              public salida: Salidas_bascula,
              public mezcla: Mezcla_bascula,
              public database: DatabaseProvider,
              public events: Events,
              public socket: Socket,
              private toastCtrl: ToastController
  ) {
    this.events.subscribe('creado', async () => {
      console.error('EVENT IS FIRED');
      await this.upload();
      console.log('dataUploaded');
    });
    this.reconnectRx = this.api.post('auth/reconnected', null);
  }

  static get onLine() {
    return NetworkProvider.onLine;
  }

  reconnect() {
    this.reconnectRx.subscribe(async (data) => {
      console.warn('\n\n\n reconnected: \n\n\n', data);
      for (let key in data) {
        const body = data[key];
        console.log(key, body);

        switch (key) {
          case 'usuarios':
            await this.usersDb.saveMany(body);
            break;
          case 'productos':
            await this.products.saveMany(body);
            break;
          case 'clientes':
            await this.clientes.saveMany(body);
            break;
          case 'materiales':
            await this.materiales.saveMany(body);
            break;
          case 'tipos_vehiculo':
            await this.tipos_vehiculos.saveMany(body);
            break;
          case 'vehiculos':
            await this.vehiculos.saveMany(body);
            break;
          case 'conductores':
            await this.conductores.saveMany(body);
            break;
          case 'vehiculos_conductores':
            await this.vehiculo_conductores.saveMany(body);
            break;
          case 'patios':
            await this.patios.saveMany(body);
            break;
          case 'pilas':
            await this.pilas.saveMany(body);
            break;
          case 'minas':
            await this.minas.saveMany(body);
            break;
          case 'mantos':
            await this.mantos.saveMany(body);
            break;
        }
      }
    })
  }

  download() {
    return new Promise((resolve, reject) => {
      if (!JSON.parse(localStorage.getItem('db-seeded'))) {
        this.database.dbReady.subscribe(() => {
          const t = this.toastCtrl.create({
            showCloseButton: true,
            message: 'Cargando datos',
            dismissOnPageChange: false,
            duration: 10000,
            position: 'bottom',
          });
          t.present();
          this.api.get('download-data')
            .subscribe(
              (data: any) => {
                const promises = [
                  this.products.saveMany(data.productos),
                  this.usersDb.saveMany(data.usuarios),
                  this.clientes.saveMany(data.clientes),
                  this.materiales.saveMany(data.materiales),
                  this.tipos_vehiculos.saveMany(data.tipos_vehiculo),
                  this.vehiculos.saveMany(data.vehiculos),
                  this.conductores.saveMany(data.conductores),
                  this.vehiculo_conductores.saveMany(data.vehiculos_conductores),
                  this.patios.saveMany(data.patios),
                  this.pilas.saveMany(data.pilas),
                  this.minas.saveMany(data.minas),
                  this.mantos.saveMany(data.mantos),
                  this.entrada.saveMany(data.entradas),
                  this.salida.saveMany(data.salidas),
                  this.mezcla.saveMany(data.mezclas)
                ];
                Promise.all(promises).then(
                  (r) => {
                    console.warn('FINISH SEED', r);
                    localStorage.setItem('db-seeded', "true");
                    return resolve(r);
                  },
                  (e) => {
                    console.error('ERROR SEED: ', e);
                    localStorage.setItem('db-seeded', "false");
                    DatabaseProvider.dbFilled = false;
                    return reject(e);
                  });
              },
              (error) => {
                console.error('ERROR DOWNLOAD', error);
                return reject(error);
              },
              () => {
                t.dismissAll();
              }
            );
        });
      }
      else {
        resolve();
      }
    });
  }

  upload() {
    return new Promise(async (resolve, reject) => {
      if (SyncProvider.onLine) {
        const syncVehicles = async () => {
          const vehiculos = await this.vehiculos.forUpload();
          console.warn('VEHICULOS PARA ACTUALIZAR', vehiculos);
          if (vehiculos.length > 0) {
            const resultados = await this.api.post('upload-data/vehiculos', vehiculos).toPromise();
            console.log('AFTER UPLOAD VEHICULOS', resultados);
            for (let v in resultados) {
              console.log('WHERE IS My Vehicle?', resultados, v, resultados[v]);
              const afterUpdateVehicle = await this.vehiculos.updateIds(resultados[v].id, resultados[v].oldId);
              console.log('VEHICULO ACTUALIZADO', afterUpdateVehicle);
            }
          }
        };

        const syncDrivers = async () => {
          const forUpload = await this.conductores.forUpload();
          // console.warn('CONDUCTORES PARA ACTUALIZAR', forUpload);
          if (forUpload.length > 0) {
            const resultados: any = (await this.api.post('upload-data/conductores', forUpload).toPromise());
            console.log('AFTER UPLOAD CONDUCTORES', resultados);
            for (let v of resultados) {
              console.log('WHERE IS My Driver?', v, resultados);
              const afterUpdate = await this.conductores.updateIds(v.id, v.oldId);
              console.log('CONDUCTOR ACTUALIZADO', afterUpdate);
            }
          }
        };

        const syncVC = async () => {
          const forUpload = await this.vehiculo_conductores.forUpload();
          // console.warn('VC PARA ACTUALIZAR', forUpload);
          if (forUpload.length > 0) {
            const resultados = await this.api.post('upload-data/vehiculos-conductores', forUpload).toPromise();
            // console.log('AFTER UPLOAD vc', resultados);
            for (let v in resultados) {
              // console.log('WHERE IS My vc?', v, resultados, resultados[v]);
              const afterUpdate = await this.vehiculo_conductores.updateIds(resultados[v].id, resultados[v].oldId);
              console.log('VC ACTUALIZADO', afterUpdate);
            }
          }
        };

        const syncEntries = async () => {
          console.log('\n\n\n\n\n\n\n\n');
          const forUpload = await this.entrada.forUpload();
          console.warn('Entradas PARA ACTUALIZAR', forUpload);
          if (forUpload.length > 0) {
            const resultados = await this.api.post('upload-data/entradas-bascula', forUpload).toPromise();
            console.log('AFTER UPLOAD entradas', resultados);
            for (let v in resultados) {
              console.log('WHERE IS My bascule entry?', v, resultados, resultados[v]);
              const afterUpdate = await this.entrada.updateIds(resultados[v].id, resultados[v].oldId);
              console.log('entrada ACTUALIZADA', afterUpdate);
            }
          }
          console.log('\n\n\n\n\n\n\n\n');
        };

        const syncOuts = async () => {
          const forUpload = await this.salida.forUpload();
          console.warn('salidas PARA ACTUALIZAR', forUpload);
          if (forUpload.length > 0) {
            const resultados = await this.api.post('upload-data/salidas-bascula', forUpload).toPromise();
            // console.log('AFTER UPLOAD salidas', resultados);
            for (let v in resultados) {
              // console.log('WHERE IS My bascule out?', v, resultados, resultados[v]);
              const afterUpdate = await this.salida.updateIds(resultados[v].id, resultados[v].oldId);
              console.log('salida ACTUALIZADA', afterUpdate);
            }
          }
        };

        try {
          this.database.dbReady.pipe(distinctUntilChanged()).subscribe((value) => {
            if (value) {
              syncVehicles().then(
                () => {
                  // console.log('VEHICULOS 1', v);
                  syncDrivers().then(
                    () => {
                      // console.log('CONDUCTORES 1', c);
                      syncVC().then(
                        () => {
                          // console.log('VEHICULOS_CONDUCTORES 1', vc);
                          syncEntries().then(
                            () => {
                              // console.log('ENTRADAS BASCULA 1', eb);
                              resolve(null);
                            },
                            (e) => {
                              console.error(e);
                            }
                          );
                          syncOuts().then(
                            () => {
                              // console.log('SALIDAS BASCULA 1', sb);
                              resolve(null);
                            },
                            (e) => {
                              console.error(e);
                            }
                          );
                        },
                        (e) => {
                          console.error(e);
                        }
                      );
                    },
                    (e) => {
                      console.error(e);
                    }
                  );
                },
                (e) => {
                  console.error(e);
                }
              );
              // console.warn('ENTRADAS PARA ACTUALIZAR', await this.entrada.forUpload());
              // console.warn('SALIDAS PARA ACTUALIZAR', await this.salida.forUpload());
            }
          });
        } catch (e) {
          return reject(e);
        }
      }
      resolve(null);

    });
  }

  createToast(message: string) {
    const toast = this.toastCtrl.create({
      showCloseButton: true,
      dismissOnPageChange: false,
      message,
      position: 'bottom',
      duration: 6000,
      closeButtonText: 'X',
    });
    return toast.present();
  }

  socketConnect() {
    this.socket.connect();


    this.socket.on('connect', () => {
      this.reconnect();
    });


    if (!this.onNews) {
      this.onNews = this.socket.fromEvent('new-data').share();
      this.onNews.pipe(
        tap((data) => {
          this.createToast(`Actualizando...`);
          console.log('\n\n\n\n\n\n\n\n\n\n');
          console.log(data);
          console.log('\n\n\n\n\n\n\n\n\n\n');
        }),
        delay(600)
      ).subscribe(
        async (data: any) => {
          const user = AuthService.currentUser.id;
          const body = _.isArray(data.data) ? data.data : [data.data];
          if (data.user !== user) {
          console.log('Sync online Data', data);
          switch (data.entity) {
            case 'usuarios':
              await this.usersDb.saveMany(body);
              break;
            case 'productos':
              await this.products.saveMany(body);
              break;
            case 'clientes':
              await this.clientes.saveMany(body);
              break;
            case 'materiales':
              await this.materiales.saveMany(body);
              break;
            case 'tipos_vehiculo':
              await this.tipos_vehiculos.saveMany(body);
              break;
            case 'vehiculos':
              console.log('\n\n\n\n\n\n\n', body, '\n\n\n\n\n\n\n');
              await this.vehiculos.saveMany(body);
              break;
            case 'conductores':
              await this.conductores.saveMany(body);
              break;
            case 'vehiculos_conductores':
              await this.vehiculo_conductores.saveMany(body);
              break;
            case 'patios':
              await this.patios.saveMany(body);
              break;
            case 'pilas':
              await this.pilas.saveMany(body);
              break;
            case 'minas':
              await this.minas.saveMany(body);
              break;
            case 'mantos':
              await this.mantos.saveMany(body);
              break;
          }
          this.events.publish('new-data', data.entity);
          }
        });
    }

  }
}
