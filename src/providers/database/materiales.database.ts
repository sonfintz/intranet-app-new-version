import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "./database";
import {Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {mapRows} from "../../helpers/db";

@Injectable()
export class MaterialesDatabase {
  db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean>;

  constructor(public sqlite: SQLite,
              public database: DatabaseProvider,
              platform: Platform) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.dbReady.next(true);
          this.db = db;
        })
    });
  }

  private isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  findAll() {
    return this.isReady().then(() => {
      return this.db.executeSql(`SELECT * FROM materiales;`, [])
        .then(mapRows)
        .catch((e) => {
          console.error('HA ocurrido un error en la consulta findAll:', e);
          return [];
        });
    });
  }

  async saveMany(many: any[]) {
    const results = [];
    try {
      await this.isReady();
      for (const key in many){
        const data = many[key];
        const exists = mapRows(await this.db.executeSql('select id from materiales WHERE id = ? LIMIT 1',
          [data.id]));
        if (!(exists.length > 0)) {
          const values = Object.keys(data).map((key) => data[key]);
              const arr = new Array(values.length).join('?, ') + '?';
              const sql = `INSERT INTO materiales VALUES(${ arr });`;
              results.push(await this.db.executeSql(sql, values));
        }
      }
    } catch (e) {
      console.error('HAY UN ERROR AL GUARDAR EL MATERIAL', e)
    }
    return results;
  }

}
