import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "./database";
import {Events, Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {mapRows} from "../../helpers/db";
import {IEntry} from "../../models/entry";
import {AuthService} from "../_core/auth-service/auth-service";
import {Vehiculos_conductoresDatabase} from "./vehiculos_conductores.database";

@Injectable()
export class Entrada_bascula {
  db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean>;

  constructor(public sqlite: SQLite,
              public database: DatabaseProvider,
              public events: Events,
              public vcDB: Vehiculos_conductoresDatabase,
              platform: Platform) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.dbReady.next(true);
          this.db = db;
        })
    });
  }

  isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  findAll() {
    return this.isReady().then(() => {
      return this.db.executeSql(`
        SELECT 
          eb.*,
          p.nombre as producto_nombre,
          v.placa as vehiculo_placa,
          pi.nombre as pila_nombre,
          c.nombre as conductor_nombre,
          ma.nombre as manto_nombre,
          mi.nombre as mina_nombre,
          pa.nombre as patio_nombre,
          c.dni as conductor_dni
        FROM entradas_bascula eb 
        JOIN productos p ON (p.id = eb.productos_id) 
        JOIN conductores c ON (c.id = eb.vc_conductores_id) 
        JOIN vehiculos v ON (v.id = eb.vc_vehiculos_id) 
        JOIN mantos ma ON (ma.id = eb.mantos_id) 
        JOIN minas mi ON (mi.id = ma.minas_id) 
        JOIN patios pa ON (pa.id = eb.patios_id) 
        JOIN pilas pi ON (pi.id = eb.pilas_id) 
        ORDER BY eb.creado desc`, [])
        .then(mapRows, (e) => {
          console.error('HA ocurrido un error en la consulta findAll ENTRADAS:', e);
          return [];
        });
    });
  }

  async forUpload() {
    await this.isReady();
    return mapRows(await this.db.executeSql('SELECT * FROM entradas_bascula WHERE (confirmed = 0 OR uploaded = 0) AND tara IS NOT NULL', []));
  }

  async updateIds(newId: number, oldId: number) {
    await this.isReady();
    if (newId === oldId) {
      await this.db.executeSql(`UPDATE entradas_bascula SET uploaded = 1, confirmed = 1  WHERE id = ?`, [oldId]);
    } else {
      await this.db.executeSql(`UPDATE entradas_bascula SET id = ?, uploaded = 1, confirmed = 1  WHERE id = ?`, [newId, oldId]);
    }
    return mapRows(await this.db.executeSql(`SELECT * FROM entradas_bascula WHERE id = ?`, [newId]));
  }

  findByField(field: string, value: string | number, op = '=') {
    return new Promise((resolve, reject) => {
      this.isReady().then(() => {
        this.db.executeSql(`SELECT * FROM entradas_bascula WHERE eliminado IS NULL AND ${field} ${op} ? `, [value])
          .then(
            (r) => resolve(mapRows(r)),
            (e) => reject(e),
          )
      });
    })

  }

  async getId() {
    await this.isReady();
    return (mapRows(await this.db.executeSql(`SELECT IFNULL((MIN(id) -1), -1) id FROM entradas_bascula where id < 0;`, []))[0]).id;
  }

  async save(body: IEntry) {
    await this.isReady();
    const user = AuthService.currentUser;
    let vcList = mapRows(await this.vcDB.db.executeSql('SELECT * FROM vehiculos_conductores WHERE vehiculos_id = ? AND conductores_id = ? AND estado = 1 LIMIT 1', [body.vehiculo.id, body.conductor.id]));
    let vc: any;
    if (vcList.length> 0) {
      vc = vcList[0];
    } else {
      const temp = await this.vcDB.onlySave(body.vehiculo.id, body.conductor.id);
      vc = {
        id: temp.vc_id,
        conductores_id: temp.c_id,
        vehiculos_id: temp.v_id
      };
      console.log('\n\n\n\n\n');
      console.log('GUARDADO NO ASIGNADO', vc);
      console.log('\n\n\n\n\n');
    }
    const id = await this.getId();
    console.log('PARAMETROS', [
      id, body.neto, body.bruto, (isNaN(body.tara)?undefined:body.tara), (body.observacion || null),
      body.productos_id, body.pilas_id, body.patios_id, user.sede.id,
      body.mantos_id, user.id, vc.id, vc.vehiculos_id, vc.conductores_id
    ]);

    await this.db.executeSql(`INSERT INTO entradas_bascula(
      id, neto, bruto, tara, fecha, fecha_registro, observacion,
      productos_id, pilas_id, patios_id, sedes_id,
      mantos_id, usuarios_id,
      vc_id, vc_vehiculos_id, vc_conductores_id
    ) VALUES(?, ?, ?, ?, (datetime("now", "localtime")), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, [
      id, body.neto, body.bruto, (isNaN(body.tara)?undefined:body.tara),  body.fecha_registro, (body.observacion || null),
      body.productos_id, body.pilas_id, body.patios_id, user.sede.id,
      body.mantos_id, user.id, vc.id, vc.vehiculos_id, vc.conductores_id
    ]);
    const resultados = await this.db.executeSql('SELECT * FROM entradas_bascula WHERE id = ?', [id]);
    console.warn('\n\n\n\n\n Entrada registrada', resultados, '\n\n\n\n\n\n');
    return mapRows(resultados)[0];
  }

  saveMany(many: any[]) {
    const promises = [];
    console.log('ENTRADAS DE BASCULA.');
    many.forEach((data) => {
      const values = Object.keys(data).map((key) => data[key]);
      const sql = `INSERT INTO entradas_bascula VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, 1);`;
      promises.push(this.db.executeSql(sql, [...values]));
    });
    return Promise.all(promises).then(
      (r) => {
        console.warn('THEN ENTRADAS', r);
        return r;
      },
      (e) => {
        console.warn('CATCH ENTRADAS', e);
        return e
      }
    );
  }

  async update(id: number, tara: number, bruto:number) {
    console.log('ERROR',id,tara);
    await this.isReady();
    await this.db.executeSql(`UPDATE entradas_bascula SET tara = ?, neto = ? WHERE id = ?`, [tara, (bruto-tara), id]);
    return await this.findByField('id', id);
  }

}
