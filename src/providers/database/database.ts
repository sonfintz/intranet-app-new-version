import 'rxjs/add/operator/share';
import 'rxjs/add/operator/toPromise';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {Injectable} from "@angular/core";
import {Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {SQLitePorter} from "@ionic-native/sqlite-porter";
import {dump} from "../../config/dumps/sql";

@Injectable()
export class DatabaseProvider {

  private databaseReady: BehaviorSubject<boolean>;
  database: SQLiteObject;

  static set dbFilled(val) {
    localStorage.setItem('db-filled', JSON.stringify(val));
  }
  static get dbFilled() {
    return JSON.parse(localStorage.getItem('db-filled'))
  }

  get dbReady() {
    return this.databaseReady.asObservable();
  }

  constructor(private sqlite: SQLite,
              private platform: Platform,
              private sqlitePorter: SQLitePorter
  ) {
    this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
        this.database = db;
        const dbFilled = DatabaseProvider.dbFilled;
        if (dbFilled) {
          this.databaseReady.next(true);
        } else {
          this.fillDatabase(db);
        }
      });
    });
  }

  fillDatabase(db?) {
    this.databaseReady.next(false);
    this.sqlitePorter.importSqlToDb(db || this.database, dump)
      .then((r) => {
        DatabaseProvider.dbFilled = true;
        this.databaseReady.next(true);
        console.warn('Tables Seeded', r);
      })
      .catch(e => {
        DatabaseProvider.dbFilled = false;
        this.databaseReady.next(false);
        console.error('Tables Seeded ERROR', e);
      });
  }
}

