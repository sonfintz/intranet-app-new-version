import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {Events, Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {mapRows} from "../../helpers/db";
import {AuthService} from "../_core/auth-service/auth-service";
import {IOut} from "../../models/out";
import {Vehiculos_conductoresDatabase} from "./vehiculos_conductores.database";

@Injectable()
export class Salidas_bascula {
  db: SQLiteObject;
  dbReady: BehaviorSubject<boolean>;

  constructor(public sqlite: SQLite,
              public events: Events,
              platform: Platform,
              public vcDB: Vehiculos_conductoresDatabase,
  ) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.dbReady.next(true);
          this.db = db;
        })
    });
  }

  private isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  findAll() {
    return this.isReady().then(() => {
      return this.db.executeSql(`
      SELECT 
      sb.*, 
      m.nombre as material_nombre,
      v.placa as vehiculo_placa,
      c.nombre as conductor_nombre,
      c.dni as conductor_dni,
      pi.nombre as pila_nombre,
      pa.nombre as patio_nombre,
      mi.nombre as mina_nombre,
      cl.nombre as clientes_nombre
      FROM salidas_bascula sb 
      JOIN materiales m ON (m.id = sb.materiales_id)
      JOIN minas mi ON (mi.id = sb.minas_id)
      JOIN conductores c ON (c.id = sb.vc_conductores_id) 
      JOIN vehiculos v ON (v.id = sb.vc_vehiculos_id) 
      JOIN clientes cl ON (cl.id = sb.clientes_id)
      JOIN patios pa ON (pa.id = sb.patios_id) 
      JOIN pilas pi ON (pi.id = sb.pilas_id)
      ORDER BY sb.creado desc`, [])
        .then(mapRows, (e) => {
          console.error('HA ocurrido un error en la consulta findAll SALIDAS:', e);
          return [];
        });
    });
  }

  async forUpload() {
    await this.isReady();
    return mapRows(await this.db.executeSql('SELECT * FROM salidas_bascula WHERE (confirmed = 0 OR uploaded = 0 ) AND tara IS NOT NULL', []));
  }

  async updateIds(newId: number, oldId: number) {
    await this.isReady();

    if (newId === oldId) {
      await this.db.executeSql(`UPDATE salidas_bascula SET uploaded = 1, confirmed = 1  WHERE id = ?`, [oldId]);
    } else {
      await this.db.executeSql(`UPDATE salidas_bascula SET id = ?, uploaded = 1, confirmed = 1  WHERE id = ?`, [newId, oldId]);
    }
    return mapRows(await this.db.executeSql(`SELECT * FROM salidas_bascula WHERE id = ?`, [newId]));
  }

  findByField(field: string, value: string | number, op = '='): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.isReady().then(() => {
        this.db.executeSql(`SELECT * FROM salidas_bascula WHERE ${field} ${op} ?;`, [value])
          .then(
            value1 => resolve(mapRows(value1)),
            error => reject(error));
      });
    });
  }

  async getId() {
    await this.isReady();
    return (mapRows(await this.db.executeSql(`SELECT IFNULL((MIN(id) -1), -1) id FROM salidas_bascula where id < 0;`, []))[0]).id;
  }

  async save(body: IOut) {
    await this.isReady();
    const user = AuthService.currentUser;
    let vcList = mapRows(await this.vcDB.db.executeSql('SELECT * FROM vehiculos_conductores WHERE vehiculos_id = ? AND conductores_id = ? AND estado = 1 LIMIT 1', [body.vehiculo.id, body.conductor.id]));
    let vc: any;
    if (vcList.length> 0) {
      vc = vcList[0];
    } else {
      const temp = await this.vcDB.onlySave(body.vehiculo.id, body.conductor.id);
      vc = {
        id: temp.vc_id,
        conductores_id: temp.c_id,
        vehiculos_id: temp.v_id
      };
      console.log('\n\n\n\n\n');
      console.log('GUARDADO NO ASIGNADO', vc);
      console.log('\n\n\n\n\n');
    }
    const id = await this.getId();
    console.log('PARAMETROS', [
      id, body.recibo, body.neto, body.bruto, (isNaN(body.tara)?undefined:body.tara), (body.observacion || null),
      body.clientes_id, body.materiales_id, body.pilas_id, body.patios_id, user.sede.id, user.id, vc.id, vc.vehiculos_id, vc.conductores_id
    ]);

    await this.db.executeSql(`
              INSERT INTO salidas_bascula(
                id, recibo, neto, bruto,
                tara, fecha , observacion, creado, transportadora, fecha_registro,
                actualizado, clientes_id, materiales_id, pilas_id, patios_id, sedes_id,
                usuarios_id, vc_id, vc_vehiculos_id, vc_conductores_id, minas_id
              )
              VALUES(
                ?, ?, ?, ?, ?, (datetime("now", "localtime")), ?, (datetime("now", "localtime")), ?, ?, (datetime("now", "localtime")), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
              )
              `, [
      id, body.recibo, body.neto, body.bruto, (isNaN(body.tara)?undefined:body.tara), (body.observacion || null), body.transportadora, body.fecha_registro,
      body.clientes_id, body.materiales_id,  body.pilas_id, body.patios_id, user.sede.id, user.id, vc.id, vc.vehiculos_id, vc.conductores_id, body.minas_id
    ]);

    const resultados = await this.db.executeSql('SELECT * FROM salidas_bascula WHERE id = ?', [id]);
    console.warn('\n\n\n\n\n Salida registrada', resultados, '\n\n\n\n\n\n');
    return mapRows(resultados)[0];
  }

  async update(id: number, tara: number, bruto:number) {
    await this.isReady();
    console.log('ERROR',id,tara);
    await this.isReady();
    await this.db.executeSql(`UPDATE salidas_bascula SET tara = ?, neto = ? WHERE id = ?`, [tara, (bruto-tara), id]);
    return await this.findByField('id', id);
  }

  saveMany(many: any[]) {
    return this.isReady().then(() => {
      const promises = [];
      console.warn('salidas de bascula');
      many.forEach((data) => {
        const values = Object.keys(data).map((key) => data[key]);
        values.push(1, 1);
        console.log('salidas de bascula', JSON.stringify(data), values);
        const sql = `INSERT INTO salidas_bascula VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
        promises.push(this.db.executeSql(sql, values));
      });
      return Promise.all(promises).then(
        (r) => {
          console.warn('THEN SALIDAS', r);
          return r;
        },
        (e) => {
          console.warn('CATCH SALIDAS', e);
          return e
        }
      );
    });

  }

}
