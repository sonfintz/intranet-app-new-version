import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "./database";
import {Events, Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {findOne, mapRows} from "../../helpers/db";
import {VehiculosDatabase} from "./vehiculos.database";
import {ConductoresDatabase} from "./conductores.database";

@Injectable()
export class Vehiculos_conductoresDatabase {
  db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean>;

  constructor(public sqlite: SQLite,
              public database: DatabaseProvider,
              public events: Events,
              public vehiculos: VehiculosDatabase,
              public conductores: ConductoresDatabase,
              platform: Platform) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.db = db;
          this.dbReady.next(true);
        })
    });
  }

  private isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  findAll() {
    return this.isReady().then(() => {
      return this.db.executeSql(`SELECT * FROM vehiculos_conductores;`, [])
        .then(mapRows)
        .catch((e) => {
          console.error('HA ocurrido un error en la consulta findAll:', e);
          return [];
        });
    });
  }

  getDriver(vehiculo: number) {
    console.log('vehiculo_conductor', vehiculo);
    return this.isReady().then(() => {
      return this.db.executeSql(
        `
          SELECT 
            c.*,
            vc.id vc_id 
          FROM vehiculos_conductores vc
          JOIN conductores c ON (c.id = vc.conductores_id)
          WHERE vc.estado = 1 AND vc.vehiculos_id = ?
          ORDER BY date(fecha) DESC 
          LIMIT 1;
         `,
        [vehiculo]
      )
        .then(findOne)
        .catch((e) => {
          console.log('HAS ERROR ON GETDRIVER', e);
          return [];
        })
    });
  }

  async getId() {
    await this.isReady();
    return (mapRows(await this.db.executeSql(`SELECT IFNULL((MIN(id) -1), -1) id FROM vehiculos_conductores where id < 0;`, []))[0]).id;
  }

  async findById(id) {
    await this.isReady();
    return mapRows(await this.db.executeSql('SELECT * FROM vehiculos_conductores WHERE id = ?', [id]))[0]
  }

  async forUpload() {
    await this.isReady();
    return mapRows(await this.db.executeSql('SELECT * FROM vehiculos_conductores WHERE confirmed = 0 OR uploaded = 0', []));
  }

  async save(body: { vehiculo: any, conductor: any }): Promise<any> {
    const vehiculo = await this.vehiculos.save(body.vehiculo);
    const conductor = body.conductor.id ? body.conductor : await this.conductores.save(body.conductor);
    const existRelation = await this.findWithRelations(vehiculo.id, conductor.id);
    if (existRelation.length > 0) {
      return existRelation[0];
    } else {
      return await this.onlySave(vehiculo.id, conductor.id);
    }
  }

  async onlySave(vehiculo: number, conductor: number) {
    await this.db.executeSql(`UPDATE vehiculos_conductores SET estado = 0 WHERE ((vehiculos_id = ?) OR (conductores_id = ?) ) AND estado = 1`, [vehiculo, conductor]);
    const id = await this.getId();
    await this.db.executeSql(`INSERT INTO vehiculos_conductores(id,  vehiculos_id,  conductores_id,  fecha, estado) VALUES (?, ?, ?, (datetime("now", "localtime")), 1)`, [id, vehiculo, conductor]);
    return await this.findWithRelationsById(id);
  }

  async findWithRelations(vehiculo: number, conductor: number) {
    try {
      await this.isReady();
      const sql = `
      SELECT 
        vc.id vc_id,
        vc.fecha vc_fecha,
        vc.estado vc_estado,
        v.id v_id,
        v.placa v_placa,
        v.tipos_vehiculo_id v_tipos_vehiculo_id,
        c.id c_id,
        c.nombre c_nombre,
        c.dni c_dni
      FROM vehiculos_conductores vc
      JOIN vehiculos v ON (vc.vehiculos_id = v.id)
      JOIN conductores c ON (vc.conductores_id = c.id)
      WHERE vc.estado = 1 AND v.id = ? AND c.id = ?;`;
      return mapRows(await this.db.executeSql(sql, [vehiculo, conductor]))
    } catch (e) {
      console.error('HA ocurrido un error en la consulta findAll:', e);
      return [];
    }
  }
  async findWithRelationsById(id: number) {
    try {
      await this.isReady();
      const sql = `
      SELECT 
        vc.id vc_id,
        vc.fecha vc_fecha,
        vc.estado vc_estado,
        v.id v_id,
        v.placa v_placa,
        v.tipos_vehiculo_id v_tipos_vehiculo_id,
        c.id c_id,
        c.nombre c_nombre,
        c.dni c_dni
      FROM vehiculos_conductores vc
      JOIN vehiculos v ON (vc.vehiculos_id = v.id)
      JOIN conductores c ON (vc.conductores_id = c.id)
      WHERE vc.id = ?;`;
      return mapRows(await this.db.executeSql(sql, [id]))[0];
    } catch (e) {
      console.error('HA ocurrido un error en la consulta findWithRelationsById:', e);
      return [];
    }
  }

  async saveMany(many: any[]) {
    const results = [];
    try {
      for (const key in many) {
        const data = many[key];
        const exists = mapRows(await this.db.executeSql('select id from vehiculos_conductores WHERE id = ? LIMIT 1',
          [data.id]));
        if (!(exists.length > 0)) {

          many[key].uploaded = 1;
          many[key].confirmed = 1;
          const values = Object.keys(many[key]).map((i) => many[key][i]);
          const arr = new Array(values.length).join('?, ') + '?';
          const sql = `INSERT INTO vehiculos_conductores VALUES(${ arr });`;
          results.push(await this.db.executeSql(sql, values));
        }
      }
    } catch (e) {
      const vc = await this.findAll();
      console.error('HAY UN ERROR AL GUARDAR EL VEHICULO-CONDUCTOR', e, many, vc);
    }
    return results;
  }

  async updateIds(newId: number, oldId: number) {
    await this.isReady();

    if(newId === oldId) {
      await this.db.executeSql(`UPDATE vehiculos_conductores SET uploaded = 1, confirmed = 1  WHERE id = ?`, [oldId]);
    } else{
      await this.db.executeSql(`UPDATE vehiculos_conductores SET id = ?, uploaded = 1, confirmed = 1  WHERE id = ?`, [newId, oldId]);
    }

    return mapRows(await this.db.executeSql(`SELECT * FROM vehiculos_conductores WHERE id = ?`, [newId]));
  }
}
