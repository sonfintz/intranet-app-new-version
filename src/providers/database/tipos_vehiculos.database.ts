import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "./database";
import {Events, Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {mapRows} from "../../helpers/db";

@Injectable()
export class Tipos_vehiculosDatabase {
  db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean>;
  private tabla = 'tipos_vehiculo';
  constructor(public sqlite: SQLite,
              public database: DatabaseProvider,
              public events: Events,
              platform: Platform) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.dbReady.next(true);
          this.db = db;
        })
    });
  }

  isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  async saveMany(many: any[]) {
    const results = [];
    try {
      await this.isReady();
      for (const key in many) {
        const data = many[key];
        const exists = mapRows(await this.db.executeSql('select id from tipos_vehiculo WHERE id = ? LIMIT 1',
          [data.id]));
        if (!(exists.length > 0)) {
          const values = Object.keys(many[key]).map((i) => many[key][i]);
          const arr = new Array(values.length).join('?, ') + '?';
          const sql = `INSERT INTO tipos_vehiculo VALUES(${ arr });`;
          results.push(await this.db.executeSql(sql, values));
        }
      }
    } catch (e) {
      console.error('HAY UN ERROR AL GUARDAR EL TIPO DE VEHICULO', e)
    }
    return results;
  }

  async findAll(){
    try {
      await this.isReady();
      return mapRows(await this.db.executeSql('SELECT * FROM tipos_vehiculo', []));
    } catch (e) {
      console.error(`[${this.tabla}] Ha ocurrido un error en la consulta find all:`, e);
      return [];
    }
  }
}
