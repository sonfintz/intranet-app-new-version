import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "./database";
import {Events, Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {mapRows} from "../../helpers/db";

@Injectable()
export class ConductoresDatabase {
  db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean>;
  private tabla= 'conductores';

  constructor(public sqlite: SQLite,
              public database: DatabaseProvider,
              public events: Events,
              platform: Platform) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.dbReady.next(true);
          this.db = db;
        })
    });
  }

  private isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  async getMax() {
    await await this.isReady();
    return (mapRows(await this.db.executeSql('SELECT IFNULL((MIN(id) -1), -1) as max FROM conductores where id < 0;', []))[0]).max;
  }

  async findAll() {
    try {
      await this.isReady();
      return mapRows(await this.db.executeSql(`SELECT *  FROM conductores;`, []));
    } catch (e) {
      console.error(`[${this.tabla}] Ha ocurrido un error en la consulta find all:`, e);
      return [];
    }
  }

  async findByDni(dni: string) {
    try {
      await this.isReady();
      dni = `%${dni}%`;
      return mapRows(await this.db.executeSql(`SELECT c.id, c.nombre, c.dni FROM conductores c WHERE c.dni LIKE ?;`, [dni]));
    } catch (e) {
      console.error(`[${this.tabla}] Ha ocurrido un error en la consulta find all:`, e);
      return [];
    }
  }

  findByField(field: string, value: string | number, op = '='): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.isReady().then(() => {
        this.db.executeSql(`SELECT * FROM conductores WHERE ${field} ${op} ?;`, [value])
          .then(
            value1 => resolve(mapRows(value1)),
            error => reject(error));
      });
    });
  }

  async save(body) {
    await this.isReady();
    const findDni = await this.findByField('dni', body.dni);
    if (findDni.length > 0) {
      return findDni[0];
    } else {
      const id = await this.getMax();
      await this.db.executeSql(`INSERT INTO conductores(id, dni, nombre) VALUES(?, ?, ?);`, [id, body.dni, body.nombre]);
      return mapRows(await this.db.executeSql(`SELECT * FROM conductores WHERE id = ? LIMIT 1`, [id]))[0];
    }
  }

  async forUpload() {
    await this.isReady();
    return mapRows(await this.db.executeSql('SELECT * FROM conductores WHERE confirmed = 0 OR uploaded = 0', []));
  }

  async updateIds(newId: number, oldId: number) {
    await this.isReady();
    if(newId === oldId) {
      await this.db.executeSql(`UPDATE conductores SET uploaded = 1, confirmed = 1  WHERE id = ?`, [oldId]);
    } else{
      await this.db.executeSql(`UPDATE conductores SET id = ?, uploaded = 1, confirmed = 1  WHERE id = ?`, [newId, oldId]);
    }
    return mapRows(await this.db.executeSql(`SELECT * FROM conductores WHERE id = ?`, [newId]));
  }

  async saveMany(many: any[]) {
    const results = [];

    try {
      await this.isReady();
      for (const key in many) {
        const data = many[key];
        const exists = mapRows(await this.db.executeSql('select id from conductores WHERE id = ? LIMIT 1',
          [data.id]));
        if (!(exists.length > 0)) {
          many[key].uploaded = 1;
          many[key].confirmed = 1;
          const values = Object.keys(many[key]).map((i) => many[key][i]);
          const arr = new Array(values.length).join('?, ') + '?';
          const sql = `INSERT INTO conductores VALUES(${ arr });`;
          results.push(await this.db.executeSql(sql, values));
        }
      }
    } catch (e) {
      console.error('HAY UN ERROR AL GUARDAR EL CONDUCTOR', e)
    }
    return results;
  }

}
