import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "./database";
import {Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {mapRows} from "../../helpers/db";
import {IAuthCredentials} from "../../models/user";

@Injectable()
export class UsersDatabase {
  db: SQLiteObject;
  dbReady: BehaviorSubject<boolean>;

  constructor(public sqlite: SQLite,
              public database: DatabaseProvider,
              platform: Platform) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.dbReady.next(true);
          this.db = db;
        })
    });
  }

  async saveMany(many: any[]) {
    const results = [];
    try {
      await this.isReady();
      for (const key in many) {
        const data = many[key];
        const exists = mapRows(await this.db.executeSql('select usuario_id from usuarios WHERE usuario_id = ? LIMIT 1',
          [data.id]));
        if (!(exists.length > 0)) {
          const values = Object.keys(many[key]).map((i) => many[key][i]);
          const arr = new Array(values.length).join('?, ') + '?';
          const sql = `INSERT INTO usuarios VALUES(${ arr });`;
          results.push(await this.db.executeSql(sql, values));
        }
      }
    } catch (e) {
      console.error('HAY UN ERROR AL GUARDAR EL USUARIO', e);
    }
    return results;
  }

  private isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  findAll() {
    return this.isReady().then(() => {
      return this.db.executeSql('SELECT u.* FROM usuarios u', [])
        .then(mapRows)
        .catch((e) => {
          console.error('HA ocurrido un error en la consulta findAll:', e);
          return [];
        });
    });
  }

  authentication(credentials: IAuthCredentials) {
    console.log(credentials);
    return new Promise((resolve, reject) => {
      this.isReady().then(() => {
        this.db.executeSql(
          `
            SELECT * FROM usuarios 
            WHERE usuario_pin IS NOT NULL AND (usuario_correo = ? AND usuario_pin = ?)
            LIMIT 1
            `, [credentials.correo, credentials.pin])
          .then(
            (usuarios) => {
              if (usuarios.rows.length > 0) {
                console.log(usuarios.rows.item(0));
                return resolve(usuarios.rows.item(0));
              } else {
                reject({
                  message: 'Credenciales Incorrectas.'
                })
              }
            },
            (e) => {
              reject(e);
            })
      })
    });
  }
}
