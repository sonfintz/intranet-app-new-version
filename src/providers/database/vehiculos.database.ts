import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "./database";
import {Events, Platform} from "ionic-angular";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {mapRows} from "../../helpers/db";

@Injectable()
export class VehiculosDatabase {
  db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean>;

  constructor(public sqlite: SQLite,
              public database: DatabaseProvider,
              public events: Events,
              platform: Platform) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.dbReady.next(true);
          this.db = db;
        })
    });
  }

  isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  findAll() {
    return this.isReady().then(() => {
      return this.db.executeSql(`SELECT * FROM vehiculos;`, [])
        .then(mapRows)
        .catch((e) => {
          console.error('HA ocurrido un error en la consulta findAll:', e);
          return [];
        });
    });
  }

  async getMax() {
    await await this.isReady();
    return (mapRows(await this.db.executeSql('SELECT IFNULL((MIN(id) -1), -1) as max FROM vehiculos where id < 0;', []))[0]).max;
  }

  async save(body) {
      await this.isReady();
      const findPlaca = await this.findByField('placa', body.placa);
      if (findPlaca.length > 0) {
        return findPlaca[0];
      } else {
        const id = await this.getMax();
        await this.db.executeSql(`INSERT INTO vehiculos(id, placa, tipos_vehiculo_id) VALUES( ?, ?, ?)`, [id, body.placa, body.tipos_vehiculo]);
        return mapRows(await this.db.executeSql(`SELECT * FROM vehiculos WHERE id = ? LIMIT 1`, [id]))[0];
      }
  }

  findById(id: number) {
    return this.db.executeSql(`SELECT 
          v.id vehiculo_id, 
          v.placa vehiculo_placa, 
          c.id conductor_id,
          c.nombre conductor_nombre,
          c.dni conductor_dni
          vc.id
        FROM vehiculos v
        JOIN vehiculos_conductores vc ON (vc.vehiculos_id = v.id)
        JOIN conductores c ON (vc.conductores_id = c.id) 
        WHERE eliminado IS NULL AND id = ?`, [id]);
  }

  async forUpload() {
    await this.isReady();
    return mapRows(await this.db.executeSql('SELECT * FROM vehiculos WHERE confirmed = 0 OR uploaded = 0', []));
  }

  async findByField(field: string, value: string | number, op = '=') {
    try {
      await this.isReady();
      return mapRows(await this.db.executeSql(`SELECT * FROM vehiculos WHERE ${field} ${op} ?;`, [value]));
    } catch (e) {
      console.error('HA ocurrido un error en la consulta findAll:', e);
      return [];
    }
  }

  async saveMany(many: any[]) {
    const results = [];
    try {
      for (const key in many) {
        const data = many[key];

        const exists = mapRows(await this.db.executeSql('select id from vehiculos WHERE id = ? LIMIT 1',
          [data.id]));
        if (!(exists.length > 0)) {

          many[key].uploaded = 1;
          many[key].confirmed = 1;
          const values = Object.keys(many[key]).map((i) => many[key][i]);
          const arr = new Array(values.length).join('?, ') + '?';
          const sql = `INSERT INTO vehiculos VALUES(${ arr });`;
          results.push(await this.db.executeSql(sql, values));
        } else {
          console.warn('El identificador de este vehiculo ya existe', exists);
        }
      }
    } catch (e) {
      console.error('HAY UN ERROR AL GUARDAR EL VEHICULO', e)
    }
    return results;
  }

  async updateIds(newId: number, oldId: number) {
    try {
      await this.isReady();
      if(newId === oldId) {
        await this.db.executeSql(`UPDATE vehiculos SET uploaded = 1, confirmed = 1  WHERE id = ?`, [oldId]);
      } else {
        await this.db.executeSql(`UPDATE vehiculos SET id = ?, uploaded = 1, confirmed = 1  WHERE id = ?`, [newId, oldId]);
      }
      return mapRows(await this.db.executeSql(`SELECT * FROM vehiculos WHERE id = ?`, [newId]));
    } catch (e) {
      console.error('ERROR VEHICULOS', e);
      return null;
    }
  }
}
