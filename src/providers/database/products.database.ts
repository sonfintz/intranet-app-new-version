import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {DatabaseProvider} from "./database";
import {Platform} from "ionic-angular";
import {mapRows} from "../../helpers/db";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class ProductsDatabase {

  db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean>;

  constructor(public sqlite: SQLite,
              public database: DatabaseProvider,
              platform: Platform) {
    this.dbReady = new BehaviorSubject<boolean>(false);
    platform.ready().then(() => {
      this.sqlite.create({
        name: 'intranet.db',
        location: 'default'
      })
        .then((db) => {
          this.dbReady.next(true);
          this.db = db;
        })
    });
  }

  private isReady() {
    return new Promise((resolve) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) resolve();
        });
      }
    });
  }

  findAll() {
    return this.isReady().then(() => {
      return this.db.executeSql(`SELECT * FROM productos;`, [])
        .then(mapRows)
        .catch((e) => {
          console.error('HA ocurrido un error en la consulta findAll:', e);
          return [];
        });
    });
  }

  async saveMany(many: any[]) {
    const promises = [];
    try {
      await this.isReady();
      for (let i in many) {
        const data = many[i];
        const exists = mapRows(await this.db.executeSql('select id from productos WHERE id = ? LIMIT 1', [data.id]));
        if (!(exists.length > 0)) {
          const values = Object.keys(data).map((key) => data[key]);
          const arr = new Array(values.length).join('?, ') + '?';
          const sql = `INSERT INTO productos VALUES(${ arr });`;
          promises.push(await this.db.executeSql(sql, values));
        }
      }
      return promises;
    } catch (e) {
      console.error('HAY UN ERROR AL GUARDAR PRODUCTOS', e);
    }
    return promises;
  }

}
